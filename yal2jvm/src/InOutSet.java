import java.util.HashSet;

public class InOutSet {

	private HashSet<String> inSet = new HashSet<String>();
	private HashSet<String> outSet = new HashSet<String>();
	
	/**
	 * @return the in variables set
	 */
	public HashSet<String> getInSet() {
		return inSet;
	}
	/**
	 * @param useSet the in variables set to set
	 */
	public void setInSet(HashSet<String> inSet) {
		this.inSet = inSet;
	}
	/**
	 * @return the out variables set
	 */
	public HashSet<String> getOutSet() {
		return outSet;
	}
	/**
	 * @param defSet the out variables set to set
	 */
	public void setOutSet(HashSet<String> outSet) {
		this.outSet = outSet;
	}
}