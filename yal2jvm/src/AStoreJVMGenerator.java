import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AStoreJVMGenerator extends JVMGenerator {

	public AStoreJVMGenerator(Function function, String moduleName) {
		super(function, moduleName);
	}
	
	@Override
	public void gen(String name) {
		
		String patternString = ".*?\\((.*)\\)";
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(name);
		matcher.find();
		
		String[] args = matcher.group(1).split("( )*;( )*");
		
		loadValue(args[0]);
		loadValue(args[1]);
		loadValue(args[2], "I");
		JasminGenerator.write("iastore\n");
	}

}
