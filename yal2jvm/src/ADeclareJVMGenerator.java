import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ADeclareJVMGenerator extends DeclareJVMGenerator {

	public ADeclareJVMGenerator(Function function, String moduleName) {
		super(function, moduleName);
	}

	@Override
	public void writeStaticDeclaration(String[] args) {
		if (args.length == 1) {
			JasminGenerator.write(getStaticDeclaration(args[0]));
		} else {
			JasminGenerator.write(getStaticDeclaration(args[0], Integer.parseInt(args[1])));
		}
	}

	@Override
	public String getStaticDeclaration(String name, int value) {
		JasminGenerator.globalArrays.put(name,value);
		return ".field static " + name + " [I" + "\n";
	}

	@Override
	public String getStaticDeclaration(String name) {
		return ".field static " + name + " [I" + "\n";
	}
	
	@Override
	public void gen(String name) {
		
		String patternString = ".*?\\((.*)\\)";
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(name);
		matcher.find();
		
		String[] args = matcher.group(1).split("( )*;( )*");
		
		if(!loadValue(args[1],"[I")) {
			JasminGenerator.write("newarray int\n");
		}
		storeValue(args[0]);
		super.gen(name);
		
	}

}
