import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JVMGenerator {
	
	protected static final String lineBreak = "\r\n";
	protected static final String fieldSeparator = ";";
	protected static final String argsSeparator = ",";
	protected static final String termSeparator = "#";
	
	protected Function function;
	protected VariableFinder finder;
	protected String moduleName;
	
	/**
	 * Constructor for the JVMGenerator class
	 * 
	 * @param function
	 * @param moduleName
	 */
	public JVMGenerator(Function function, String moduleName) {
		this.function = function;
		this.moduleName = moduleName;
		
		this.finder = new VariableFinder(this.function);
	}
	
	/**
	 * Write to .j file the load of a value
	 * 
	 * @param name
	 */
	public boolean loadValue(String name) {
			
		if(Pattern.matches("(-)?[0-9]+",name)) {
			int constValue = Integer.parseInt(name);
			loadConstant(constValue);
		} else if(Pattern.matches("\".*\"",name)) {
			loadString(name);
		} else if(Pattern.matches(".+?\\[.*\\]",name)) {
			loadArrayAccess(name);
		} else if(Pattern.matches(".+?\\.size$", name)) {
			loadArraySize(name);
		} else if(Pattern.matches(".+?\\(.*\\)$",name)) {
			loadFunction(name,"V");
			return true;
		} else {
			String type = this.finder.getVarType(name);
			if(type.equals("scalar")) {
				loadScalarVar(name);
			} else loadArrayVar(name);
		}
		
		return false;
	}

	
	/**
	 * Write to .j file the load of a value
	 * 
	 * @param name
	 * @param returnType
	 */
	public boolean loadValue(String name, String returnType) {
		if(Pattern.matches("(-)?[0-9]+",name)) {
			int constValue = Integer.parseInt(name);
			loadConstant(constValue);
		} else if(Pattern.matches("\".*\"",name)) {
			loadString(name);
		} else if(Pattern.matches(".+?\\[.*\\]",name)) {
			loadArrayAccess(name);
		} else if(Pattern.matches(".+?\\.size$", name)) {
			loadArraySize(name);
		} else if(Pattern.matches(".+?\\(.*\\)$",name)) {
			loadFunction(name,returnType);
			return true;
		} else {
			String type = this.finder.getVarType(name);
			if(type.equals("scalar")) {
				loadScalarVar(name);
			} else loadArrayVar(name);
		}
		
		return false;
	}
	
	
	/**
	 * Invokes a function used in a operand
	 * 
	 * @param functionCall function operand
	 * @param returnType 
	 */
	private void loadFunction(String functionCall, String returnType) {
		String functionName = functionCall.split("\\(")[0];
		String patternString = ".*?\\((.*)\\)";
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(functionCall);
		matcher.find();
		String[] args = matcher.group(1).split("( )*,( )*");
		
		//creates a call string to be passed to the CallJVMGenerator
		String callString = "call("+functionName+";[";
		for (int i = 0; i < args.length - 1; i++) {
			callString+=args[i];
			callString+=",";
		}
		callString += args[args.length - 1];
		callString += "])";
		
		new CallJVMGenerator(this.function,moduleName,returnType).gen(callString);
	}

	/**
	 * Writes to the .j file the store of a value
	 * 
	 * @param name
	 */
	public void storeValue(String name) {
		if (Pattern.matches(".+?\\[(.)+\\]",name)) {
			String arrayName = name.split("\\[")[0];
			String index = name.split("\\[")[1].split("\\]")[0];
			
			loadValue(arrayName);
			loadValue(index);
		} else {
			String type = this.finder.getVarType(name);
			if(type.equals("scalar")) {
				storeScalarVar(name);
			} else storeArrayVar(name);
		}
	}
	
	/**
	 * Writes to the .j file the load of an array
	 * 
	 * @param name
	 */
	public void loadArrayVar(String name) {
		
		int register;
		if((register = this.finder.getRegister(name)) == -1) {
			JasminGenerator.write("getstatic " + this.moduleName + "/" + name + " [I\n");
		} else {
			if(JasminGenerator.MIN_REGISTER <= register
					&& register <= JasminGenerator.MAX_REGISTER) {
				JasminGenerator.write("aload_" + register + "\n");
			} else {
				JasminGenerator.write("aload " + register + "\n");
			}
		}
	}
	
	/**
	 * Writes to the .j file the load of a scalar variable
	 *
	 * @param name
	 */
	public void loadScalarVar(String name) {
		
		int register;
		if((register = this.finder.getRegister(name)) == -1) {
			JasminGenerator.write("getstatic " + this.moduleName + "/" + name + " I\n");
		} else {
			if(JasminGenerator.MIN_REGISTER <= register
					&& register <= JasminGenerator.MAX_REGISTER) {
				JasminGenerator.write("iload_" + register + "\n");
			} else {
				JasminGenerator.write("iload " + register + "\n");
			}
		}
	}
	
	
	/**
	 * Writes to the .j file the load of a scalar variable
	 * 
	 * @param register
	 */
	public void loadScalarVar(int register) {
		
		if(JasminGenerator.MIN_REGISTER <= register
				&& register <= JasminGenerator.MAX_REGISTER) {
			JasminGenerator.write("iload_" + register + "\n");
		} else {
			JasminGenerator.write("iload " + register + "\n");
		}
	}
	
	/**
	 * Writes to the .j file the store of a scalar variable
	 * 
	 * @param register
	 */
	public void storeScalarVar(int register) {
		
		if(JasminGenerator.MIN_REGISTER <= register
				&& register <= JasminGenerator.MAX_REGISTER) {
			JasminGenerator.write("istore_" + register + "\n");
		} else {
			JasminGenerator.write("istore " + register + "\n");
		}
	}
	
	/**
	 * Writes to the .j file the load of an string
	 * 
	 * @param string
	 */
	public void loadString(String string) {
		JasminGenerator.write("ldc " + string + "\n");
	}
	
	/**
	 * Writes to the .j file the store of a scalar variable
	 * 
	 * @param name
	 */
	public void storeScalarVar(String name) {
		
		int register;
		if((register = this.finder.getRegister(name)) == -1) {
			JasminGenerator.write("putstatic " + this.moduleName + "/" + name + " I\n");
		} else {
			if(JasminGenerator.MIN_REGISTER <= register
					&& register <= JasminGenerator.MAX_REGISTER) {
				JasminGenerator.write("istore_" + register + "\n");
			} else {
				JasminGenerator.write("istore " + register + "\n");
			}
		}
		
	}
	
	/**
	 * Writes to the .j file the store of an array
	 * 
	 * @param name
	 */
	public void storeArrayVar(String name) {
		
		int register;
		if((register = this.finder.getRegister(name)) == -1) {
			JasminGenerator.write("putstatic " + this.moduleName + "/" + name + " [I\n");
		} else {
			if(JasminGenerator.MIN_REGISTER <= register
					&& register <= JasminGenerator.MAX_REGISTER) {
				JasminGenerator.write("astore_" + register + "\n");
			} else {
				JasminGenerator.write("astore " + register + "\n");
			}
		}
		
	}
	
	/**
	 * Writes to the .j file the load of a constant
	 * 
	 * @param constant
	 */
	public void loadConstant(int constant) {
		if(JasminGenerator.MIN_CONST <= constant 
				&& constant <= JasminGenerator.MAX_CONST) {
			JasminGenerator.write("iconst_" + constant + "\n");
		} else {
			JasminGenerator.write("bipush " + constant + "\n");
		}
	}
	
	/**
	 * Writes to the .j file an array access
	 * 
	 * @param name
	 */
	public void loadArrayAccess(String name) {
		String patternString = "(.+?)\\[(.*)\\]";
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(name);
		matcher.find();
		
		String reference = matcher.group(1);
		String index = matcher.group(2);
		
		loadValue(reference);
		loadValue(index);
		JasminGenerator.write("iaload\n");
	}
	
	public void gen(String name) {
		JasminGenerator.write("\n");
	}
	
	/**
	 * Writes to the .j file the load of the array size of an array
	 * 
	 * @param name
	 */
	private void loadArraySize(String name) {
		String patternString = "(.)+?\\.size$";
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(name);
		matcher.find();
		
		String reference = matcher.group(1);
		
		loadValue(reference);
		JasminGenerator.write("arraylength" + "\n");
	}

}
