import java.util.LinkedHashMap;
import java.util.Map;

public class VariableFinder {
	
	private Function function;

	public VariableFinder(Function function) {
		this.function = function;
	}
	
	public boolean isGlobal(String name) {		
		return (YalParser.globalsTable.get(name) != null);
	}

	/**
	 * Returns whether a variable is function local.
	 * 
	 * @param varName the variable to look for
	 * @return whether the variable is function local
	 */
	public boolean isLocal(String varName) {
		return this.function.getVars().containsKey(varName);
	}
	
	/**
	 * @param name The variable name
	 * @return the register number if a function variable, -1 if a global variable, -2 if variable not found
	 */
	public int getRegister(String name) {
		
		Element elem;
		
		LinkedHashMap<String,Element> params = function.getParams();
		LinkedHashMap<String,Element> vars = function.getVars();
		
		//First look for variable in vars
		if ((elem = vars.get(name)) != null) {
			return elem.getRegister();
		}
	
		//Then look in params
		if((elem = params.get(name)) != null) {
			return elem.getRegister();
		}
		
		//Then look in globals
		if(isGlobal(name)) {
			return -1;
		}
		
		return -2;
	}

	/**
	 * Looks up a variable type in the symbol table of the current function
	 * and the global variables symbol table.
	 * 
	 * @param varName the variable name to lookup
	 * @return enum representing the variable type
	 */
	public Element.ElemType lookupVarType(String varName) {

		// Check if variable type is constant or string
		if(varName.matches("[0-9]+")) {
			return Element.ElemType.CONST;
		}
		
		if(varName.matches("\".*\"")) {
			return Element.ElemType.STRING;
		}
		
		LinkedHashMap<String, Element> params = this.function.getParams();
		LinkedHashMap<String, Element> vars = this.function.getVars();
		
		// First look for variable in locals
		if(vars.containsKey(varName)) {
			return vars.get(varName).parseAsElemType();
		}
		
		// Then look in function parameters
		if(params.containsKey(varName)) {
			return params.get(varName).parseAsElemType();
		}
		
		// Finally look in globals
		LinkedHashMap<String,Element> globals = YalParser.globalsTable;
		if(globals.containsKey(varName)) {
			return globals.get(varName).parseAsElemType();
		}

		return Element.ElemType.VOID;
	}
	
	/**
	 * 
	 * @param name The variable name
	 * @return The variable type if found, null if not found
	 */
	public String getVarType(String name) {
		Element elem;
		
		if(name.matches("[0-9]+")) {
			return "const";
		}
		
		if(name.matches("\".*\"")) {
			return "string";
		}
		
		LinkedHashMap<String,Element> params = function.getParams();
		LinkedHashMap<String,Element> vars = function.getVars();
		
		//First look for variable in vars
		if ((elem = vars.get(name)) != null) {
			return elem.getType();
		}
		
		//Then look in params
		if((elem = params.get(name)) != null) {
			return elem.getType();
		}
		
		//Then look in globals
		LinkedHashMap<String,Element> globals = YalParser.globalsTable;
		if((elem = globals.get(name)) != null) {
			return elem.getType();
		}
		
		return null;
	}
	
	public int getLastUsedRegister() {
		int register = -1;
		
		LinkedHashMap<String,Element> params = function.getParams();
		LinkedHashMap<String,Element> vars = function.getVars();
		
		for(Map.Entry<String,Element> entry:params.entrySet()) {
			register = entry.getValue().getRegister();
		}
		
		for(Map.Entry<String,Element> entry:vars.entrySet()) {
			register = entry.getValue().getRegister();
		}
		
		return register;
	}

}
