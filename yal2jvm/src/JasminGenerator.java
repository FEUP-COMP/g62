
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

public abstract class JasminGenerator {
	
	public final static int MAX_CONST = 5;
	public final static int MIN_CONST = 0;
	
	public final static int MAX_REGISTER = 3;
	public final static int MIN_REGISTER = 0;

	private static String moduleName;
	private static BufferedWriter out = null;
	private static BufferedReader in = null;
	
	public static HashMap<String, Integer> globalArrays = new HashMap<String, Integer>();
	private static int loopNumber = 0;
	/*
	 * Creates a file with the jasmin code
	 */
	public static void gen(SimpleNode root) throws IOException {

		// Create .j files directory structure
		YalUtility.getInstance().createDirIfNotExists(YalParser.jasminFilesPath);
		YalUtility.getInstance().createDirIfNotExists(YalParser.jasminFilesPath + "/" + YalParser.yalFilenameNoExt);
		
		JasminGenerator.moduleName = YalParser.moduleName;

		// Create .j file
		out = new BufferedWriter(new FileWriter(YalParser.buildFilePath(null, YalParser.jasminFilesPath, YalParser.jasminFileExt)));

		writeModule();
		write("\n");
		writeFunctions();

		out.close();
	}
	
	/**
	 * Writes string s to the generated .j file
	 * 
	 * @param String s
	 */
	public static void write(String s) {
		try {
			out.write(s);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Opens file to be read
	 * 
	 * @param String filename
	 * @return BufferedReader bufferedReader
	 */
	private static BufferedReader openFileToRead(String fileName) throws FileNotFoundException {
		File file = new File(fileName);
		FileReader fileReader = new FileReader(file);
		BufferedReader bufferedReader = new BufferedReader(fileReader);

		return bufferedReader;
	}
	
	/**
	 * Writes the module declaration to the generated .j file
	 */
	private static void startModule() {
		String header = ".class public " + moduleName + "\n.super java/lang/Object\n\n";
		write(header);
	}
	
	/**
	 * Converts a HIR file(.bla) module to a .j file
	 */
	private static void writeModule() throws IOException {
		
		startModule();
		
		// Open module HIR file
		in = openFileToRead(YalParser.buildFilePath(null, YalParser.hirFilesPath, YalParser.hirFileExt));
		
		String line;
		while ((line = in.readLine()) != null) {
			parseDeclarationLine(line);
		}

		write("\n");
		write(getArraysInstatiation());

		in.close();
	}
	
	/**
	 * Converts a line from a HIR file(.bla) to jasmin
	 */
	private static void parseDeclarationLine(String line) {
		String inst = line.split("\\(")[0];
		String[] args = ((line.split("\\(")[1]).split("\\)")[0]).split("( )*;( )*");
		
		DeclareJVMGenerator gen;
		if (inst.equals("ideclare")) {
			gen = new IDeclareJVMGenerator(null,moduleName);
			gen.writeStaticDeclaration(args);
		} else if (inst.equals("adeclare")) {
			gen = new ADeclareJVMGenerator(null,moduleName);
			gen.writeStaticDeclaration(args);
		}
	}

	/**
	 * Returns the clinit function to initialize the arrays
	 * 
	 * @return String arraysInstatiation
	 */
	private static String getArraysInstatiation() {

		int stackSize;

		if (globalArrays.size() == 0) {
			stackSize = 0;
		} else
			stackSize = 10;

		String returnString = ".method static private <clinit>()V\n" + ".limit stack " + stackSize + "\n"
				+ ".limit locals 0\n\n";

		Iterator<Entry<String, Integer>> it = globalArrays.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, Integer> pair = it.next();

			returnString += "bipush " + pair.getValue() + "\n" + "newarray int\n" + "putstatic " + moduleName + "/"
					+ pair.getKey() + " [I\n";
		}

		returnString += "return\n" + ".end method\n";

		return returnString;
	}
	
	/**
	 * Write all functions to the .j file
	 */
	private static void writeFunctions() throws IOException {

		HashMap<String, Function> functions = YalParser.functionTables;

		Iterator<Entry<String, Function>> it = functions.entrySet().iterator();

		while (it.hasNext()) {
			Entry<String, Function> pair = it.next();
			writeFunction(pair.getKey(), pair.getValue());
			write("\n");
		}

	}
	
	/**
	 * Writes function with functionName to the .j file
	 * 
	 * @param String functionName
	 * @param Function function
	 */
	private static void writeFunction(String functionName, Function function) throws IOException {
		
		declareFunction(functionName, function);
		function.assignRegisters();
		
		// Open function HIR file
		in = openFileToRead(YalParser.buildFilePath(functionName, YalParser.hirFilesPath, YalParser.hirFileExt));

		String line;
		while ((line = in.readLine()) != null) {
			parseLine(line, function);
		}
		
		JVMGenerator gen = new JVMGenerator(function,moduleName);
		
		Element returnElem;
		if((returnElem = function.getReturnElem()) != null) {
			if(returnElem.getType().equals("scalar")) {
				gen.loadScalarVar(returnElem.getName());
				write("i");
			} else {
				gen.loadArrayVar(returnElem.getName());
				write("a");
			}
		}
		write("return\n");
		
		write(".end method\n");

		in.close();
	}

	/**
	 * Writes the header with the function with functionName to the .j file
	 * @param String functionName
	 * @param Function function
	 */
	private static void declareFunction(String functionName, Function function) {

		int stackSize = 30; // TODO calculate this later for optimizations
		int localSize = 30;

		if (functionName.equals("main")) {
			write(".method public static main([Ljava/lang/String;)V\n");
			write(".limit locals " + localSize + "\n");
			write(".limit stack " + stackSize + "\n\n");
			return;
		}

		String[] paramTypes = getParamTypes(function);
		String returnType = getReturnType(function);

		String functionDeclaration = ".method public static " + functionName + "(";

		for (int i = 0; i < paramTypes.length; i++) {
			functionDeclaration += paramTypes[i];
		}

		functionDeclaration += ")" + returnType + "\n";

		write(functionDeclaration);
		write(".limit locals " + localSize + "\n");
		write(".limit stack " + stackSize + "\n\n");
	}
	
	/**
	 * Returns the types of the params of the function 
	 * 
	 * @param Function function
	 * @return String[] paramTypes
	 */
	private static String[] getParamTypes(Function function) {

		HashMap<String, Element> params = function.getParams();
		ArrayList<String> paramsList = new ArrayList<String>();
		
		Iterator<Entry<String, Element>> it = params.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, Element> pair = it.next();

			if (pair.getValue().getType().equals("scalar")) {
				paramsList.add("I");
			} else {
				paramsList.add("[I");
			}
		}
		return paramsList.toArray(new String[paramsList.size()]);
	}

	/**
	 * Return return type of the function
	 * 
	 * @param Function function
	 * @return String returnType
	 */
	private static String getReturnType(Function function) {
		Element returnElem = function.getReturnElem();

		if (returnElem == null) {
			return "V";
		} else if (returnElem.getType().equals("scalar")) {
			return "I";
		} else
			return "[I";
	}
	
	/**
	 * Parses and writes a line of HIR file (.bla) to the .j file
	 * 
	 * @param String line
	 * @param Function function
	 */
	private static void parseLine(String line, Function function) {
		String inst = line.split("\\(")[0];

		if (inst.equals("arith")) {
			new ArithJVMGenerator(function,moduleName).gen(line);
		} else if (inst.equals("call")) {
			new CallJVMGenerator(function,moduleName,"V").gen(line);
		} else if (inst.equals("ideclare")) {
			new IDeclareJVMGenerator(function,moduleName).gen(line);
		} else if (inst.equals("adeclare")) {
			new ADeclareJVMGenerator(function,moduleName).gen(line);
		} else if (inst.equals("astore")) {
			new AStoreJVMGenerator(function,moduleName).gen(line);
		} else if (inst.equals("bin")) {
			new BinJVMGenerator(function,moduleName).gen(line);
		} else if (inst.equals("if")) {
			int loopN = loopNumber++;
			new IfJVMGenerator(function,moduleName,loopN).gen(line);
			parseIf(function,loopN);
		} else if (inst.equals("while")) {
			int loopN = loopNumber++;
			JasminGenerator.write("loop" + loopN + ":\n\n");
			new IfJVMGenerator(function,moduleName,loopN).gen(line);
			parseWhile(function,loopN);
		} else if (inst.equals("afill")) {
			int loopN = loopNumber++;
			int registerAux = new VariableFinder(function).getLastUsedRegister() + 1;
			new AFillJVMGenerator(function,moduleName,loopN,registerAux).gen(line);
		}
	}
	
	
	/**
	 * Parses and writes a while loop of HIR file (.bla) to the .j file
	 * 
	 * @param Function function
	 * @param int number to generate loop label
	 */
	private static void parseWhile(Function function, int loopN) {
		
		String line;
		try {
			while ((line = in.readLine()) != null) {
				if(line.equals("endwhile")) {
					JasminGenerator.write("goto loop" + loopN + "\n\n");
					JasminGenerator.write("loop" + loopN + "_end:\n\n");
					break;
				}
				parseLine(line, function);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * Parses and writes a if statement of HIR file (.bla) to the .j file
	 * 
	 * @param Function function
	 * @param int number to generate loop label
	 */
	private static void parseIf(Function function, int loopNumber) {
		
		boolean hasElse = false;
		
		String line;
		try {
			while ((line = in.readLine()) != null) {
				if(line.equals("endif")) {
					if(!hasElse) {
						JasminGenerator.write("loop" + loopNumber + "_end:\n");
					} else {
						JasminGenerator.write("loop" + loopNumber + "_next:\n");
					}
					break;
				} else if (line.equals("else")) {
					hasElse = true;
					JasminGenerator.write("goto loop" + loopNumber + "_next\n");
					JasminGenerator.write("loop" + loopNumber + "_end:\n");
				}
				parseLine(line, function);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
