import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ArithJVMGenerator extends JVMGenerator {
	
	public ArithJVMGenerator(Function function, String moduleName) {
		super(function,moduleName);
	}
	
	public void gen(String line) {
		String patternString = ".*?\\((.*)\\)";
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(line);
		matcher.find();
		
		String[] args = matcher.group(1).split("( )*;( )*");
		
		String[] ops = args[1].split(JVMGenerator.termSeparator);
		
		for(int i = 0; i < ops.length; i++) {
			ops[i] = ops[i].replaceAll("\\s+","");
		}
		
		String lhs = args[0];
		
		if(Pattern.matches(".+?\\[(.)+\\]",lhs)) {
			VariableFinder finder = new VariableFinder(this.function);
			int registerAux = finder.getLastUsedRegister();
			
			loadValue(ops[0],"I");
			loadValue(ops[2],"I");
			writeOperation(ops[1]);
			storeScalarVar(registerAux);
			
			storeValue(lhs);
			loadScalarVar(registerAux);
			JasminGenerator.write("iastore");
		} else {
			if(ops[0].equals(lhs) && ops[2].equals("1") && ops[1].equals("+")) {
				int register = new VariableFinder(function).getRegister(lhs);
				JasminGenerator.write("iinc " + register + " 1\n");
			} else {
				
				loadValue(ops[0],"I");
				loadValue(ops[2],"I");
				writeOperation(ops[1]);
				storeValue(lhs);
			}
		}
		
		JasminGenerator.write("\n");
	}

	private void writeOperation(String string) {
		char op = string.charAt(0);
		
		switch(op) {
		case '+':
			JasminGenerator.write("iadd\n");
			break;
		case '-':
			JasminGenerator.write("isub\n");
			break;
		case '*':
			JasminGenerator.write("imul\n");
			break;
		case '/':
			JasminGenerator.write("idiv\n");
			break;
		}
	}
}
