import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class RegisterAllocation {

	// HIR values
	private static final String ifStart = "if";
	private static final String elseStart = "else";
	private static final String ifEnd = "endif";
	private static final String whileStart = "while";
	private static final String whileEnd = "endwhile";
	
	HashMap<String, HashMap<Integer, UseDefSet>> functionSets; // function name -> line number -> use/def sets
	HashMap<Integer, HashSet<Integer>> successorList; // line number -> successor line numbers
	HashMap<Integer, HashMap<Integer, InOutSet>> inOutSets; // iteration number -> line number -> in/out sets
	HashMap<String, IGNode> graphNodes; // node ID -> IGNode
	Deque<IGNode> nodeStack;
	
	private int localVarCount;
	private String filepath;
	private int lineCount;
	
	// Conditional branches location
	private HashMap<String, ConditionalInfo> functionBranches;
	private Deque<String> branchStack;
	
	/**
	 * Constructs a RegisterAllocation object with a specified max
	 * local variable count and the module function's use/def sets.
	 * 
	 * @param functionSets the use/def sets for the module functions
	 * @param localVarCount the max local variable count
	 */
	public RegisterAllocation(HashMap<String, HashMap<Integer, UseDefSet>> functionSets, int localVarCount) {
		this.functionSets = functionSets;
		this.localVarCount = localVarCount;
	}
	
	/**
	 * Performs register allocation by computing the use/def sets, the successor list,
	 * the in/out sets, the interference graph and then the graph coloring. Returns whether
	 * allocation was successful.
	 * 
	 * @return whether the register allocation was successful
	 */
	public boolean runAllocation() throws IOException {
		
		LinkedHashMap<String, SimpleNode> functions = YalParser.functionsAST;
		
		// Allocate registers for each function
		for(Map.Entry<String, SimpleNode> entry : functions.entrySet()) {
			
			this.filepath = YalParser.buildFilePath(entry.getKey(), YalParser.hirFilesPath, YalParser.hirFileExt);
			
			// Reset parse variables per function
			this.functionBranches = new HashMap<String, ConditionalInfo>();
			this.branchStack = new ArrayDeque<String>();
			this.successorList = new HashMap<Integer, HashSet<Integer>>();
			
			// Locate all branches for this function
			this.locateBranches(this.openFileToRead(this.filepath));
	
			// Build the successor list
			this.branchStack = new ArrayDeque<String>();
			this.buildSuccessorList(this.openFileToRead(this.filepath));

			// Build in/out sets
			this.inOutSets = new HashMap<Integer, HashMap<Integer, InOutSet>>();
			this.buildInOutSets(entry.getKey());

			// Build the interference graph
			this.graphNodes = new HashMap<String, IGNode>();
			this.buildInterferenceGraph(this.inOutSets.get(this.inOutSets.size() - 1));

			// Checks if the IG is k-colorable
			this.nodeStack = new ArrayDeque<IGNode>();
			if(!this.checkIfColorable()) {
				System.out.println("Function " + entry.getKey() + "() is not " + this.localVarCount + "-colorable!");
				return false;
			}

			// Allocate registers
			this.graphColor(entry.getKey());
			
			// Print branch info / successor list / use/def sets
			YalDebug.getInstance().printBranchInfo(this.functionBranches);
			YalDebug.getInstance().printSuccessorList(this.successorList);
			YalDebug.getInstance().printUseDefSet(entry.getKey(), this.functionSets.get(entry.getKey()));
			
			// Print in/out sets
			for(Map.Entry<Integer, HashMap<Integer, InOutSet>> inOutSet : this.inOutSets.entrySet()) {
				YalDebug.getInstance().printInOutSets(inOutSet.getKey(), inOutSet.getValue());
			}
			
			YalDebug.getInstance().printInterferenceGraph(this.graphNodes, true);
			YalDebug.getInstance().printRegisterAllocation(entry.getKey(), this.graphNodes);
		}
		
		return true;
	}
	
	/**
	 * Alters the Function's Element objects with the colored graph registers
	 * so they are used in the code generation phase.
	 * 
	 * @param functionName the function name
	 * @param registerOffset the starting register
	 */
	private void allocateRegisters(String functionName, int registerOffset) {
		
		Function func = YalParser.functionTables.get(functionName);
		
		System.out.println("Function " + functionName + "() register allocation");
		System.out.println("Params");
		
		// Allocate registers for function parameters
		int i = 0;
		for(Map.Entry<String, Element> param : func.getParams().entrySet()) {
			System.out.println("\t" + param.getKey() + ": " + i);
			param.getValue().setRegister(i);
			i++;
		}
		
		int maxRegister = 0;
		// Allocate registers for function locals
		for(Map.Entry<String, Element> local : func.getVars().entrySet()) {
			
			// Check if graph has allocated this variable
			if(this.graphNodes.containsKey(local.getKey())) {
				
				int register = this.graphNodes.get(local.getKey()).getRegister();
				
				if(register > maxRegister) maxRegister = register;
				System.out.println("\t" + local.getKey() + ": " + register);
				local.getValue().setRegister(register);
			// If graph doesn't contain variable then allocate first non param register
			} else {
				
				if(registerOffset > maxRegister) maxRegister = registerOffset;
				System.out.println("\t" + local.getKey() + ": " + registerOffset);
				local.getValue().setRegister(registerOffset);
			}
		}
		
		System.out.println("Used " + (maxRegister + 1 - registerOffset) + " register for coloring");
	}
	
	/**
	 * Colors the graph, starting at the minimum available
	 * register considering function parameters.
	 * 
	 * @param functionName the function name
	 */
	private void graphColor(String functionName) {
		
		Function func = YalParser.functionTables.get(functionName);
		
		// Find out register offset for the locals
		int registerOffset = 0;
		if(functionName.equals("main")) {
			registerOffset = 1;
		} else {
			registerOffset = func.getParams().size();
		}
		
		if(this.nodeStack.size() < 1) {
			this.allocateRegisters(functionName, registerOffset);
			return;
		}
		
		// First node coloring
		IGNode node = this.nodeStack.pop();
		node.setActive(true);
		node.setRegister(registerOffset);
		this.graphNodes.put(node.getId(), node);
		
		// Color nodes until stack is empty
		while(!this.nodeStack.isEmpty()) {
			
			node = this.nodeStack.pop();
			node.setActive(true);
			this.chooseColor(node, registerOffset);
			this.graphNodes.put(node.getId(), node);
		}
		
		// Use register allocation info to alter Element objects to be used in code generation
		this.allocateRegisters(functionName, registerOffset);
	}

	/**
	 * Chooses the next available color for this node.
	 * 
	 * @param node the graph node to color
	 * @param registerOffset the starting register
	 */
	private void chooseColor(IGNode node, int registerOffset) {
		
		HashSet<Integer> collidingRegisters = new HashSet<Integer>();
		
		// For each interfering node create a set of interfering colors
		for(IGNode interfering : node.getInterferingNodes()) {
			
			// If node is active and has a register add it to interfering color
			if(interfering.isActive() && interfering.getRegister() != -1) {
				collidingRegisters.add(interfering.getRegister());
			}
		}
		
		// Look for next available register
		int register = registerOffset;
		while(collidingRegisters.contains(register)) {
			register++;
		}
		
		node.setRegister(register);
	}
	
	/**
	 * Tries to color the graph with the amount of locals supplied as
	 * command line argument.
	 * 
	 * @return whether the graph is k-colorable
	 */
	private boolean checkIfColorable() {

		boolean success = false;

		// Run until graph is entirely removed or no more changes to graph occur
		while(true) {

			Iterator<Map.Entry<String, IGNode>> entries = this.graphNodes.entrySet().iterator();
			boolean changed = false;
			
			// For each current node check if degree < k
			while(entries.hasNext()) {

				Map.Entry<String, IGNode> entry = entries.next();

				// Count connecting nodes
				int connecting = 0;
				for(IGNode node : entry.getValue().getInterferingNodes()) {
					if(node.isActive()) {
						connecting++;
					}
				}

				// Remove if degree is lower than total amount of colors
				if(connecting < this.localVarCount) {

					// Push node to stack
					this.nodeStack.push(this.graphNodes.get(entry.getKey()));
					
					// Set node as inactive and remove it from graph
					this.graphNodes.get(entry.getKey()).setActive(false);
					entries.remove();
					changed = true;
				}
			}

			// Successful coloring if all nodes are removed
			if(this.graphNodes.size() == 0) {
				success = true;
				break;
			// No changes on last iteration, graph is not k-colorable
			} else if(!changed) break;
		}

		return success;
	}
	
	/**
	 * Builds the interference graph for the current function.
	 * 
	 * @param inOutSets the in/out sets for each line for the current function
	 */
	private void buildInterferenceGraph(HashMap<Integer, InOutSet> inOutSets) {
		
		// Create one node per scalar variable
		for(Map.Entry<Integer, InOutSet> entry : inOutSets.entrySet()) {
			
			InOutSet inOut = entry.getValue();
			
			for(String s : inOut.getInSet()) {
				this.graphNodes.put(s, new IGNode(s));
			}
			
			for(String s : inOut.getOutSet()) {
				this.graphNodes.put(s, new IGNode(s));
			}
		}
		
		// Add interfering nodes per line
		for(Map.Entry<Integer, InOutSet> entry : inOutSets.entrySet()) {
			
			InOutSet inOut = entry.getValue();
			
			for(String s : inOut.getInSet()) {
				
				// Add every other node as interfering
				for(String key : inOut.getInSet()) {
					
					if(s == key) continue;
					
					this.graphNodes.get(s).getInterferingNodes().add(this.graphNodes.get(key));
				}
			}
			
			for(String s : inOut.getOutSet()) {
				
				// Add every other node as interfering
				for(String key : inOut.getOutSet()) {
					
					if(s == key) continue;
					
					this.graphNodes.get(s).getInterferingNodes().add(this.graphNodes.get(key));
				}
			}
		}
	}
	
	/**
	 * Builds the in/out sets for a given function. Stops
	 * when the iterations repeat.
	 * 
	 * @param functionName the function name
	 */
	private void buildInOutSets(String functionName) {
		
		int iterationCount = 0;

		// Repeat in/out set until iterations stop changing
		while(true) {

			// Add iteration to map and populate with in/out sets
			this.inOutSets.put(iterationCount, new HashMap<Integer, InOutSet>());
			this.populateInOutSet(iterationCount);

			// Start backwards
			int currentLine = this.lineCount;

			// Build in/out sets going backwards through the lines
			while(currentLine > 0) {

				// Set out set for this line
				this.inOutSets.get(iterationCount).get(currentLine).setOutSet(this.buildOutSet(iterationCount, currentLine));
				this.inOutSets.get(iterationCount).get(currentLine).setInSet(this.buildInSet(functionName, iterationCount, currentLine));

				currentLine--;
			}

			// Check if iterations match for stopping
			if(iterationCount > 0) {
				if(this.checkInOutComplete(iterationCount)) break;
			}
			
			iterationCount++;
		}
	}

	/**
	 * Verifies if the stop condition for the in/out set
	 * algorithm has been reached.
	 * 
	 * @param iterationCount the current iteration number
	 * @return whether the algorithm has reached its end
	 */
	private boolean checkInOutComplete(int iterationCount) {

		// Get the maps for the current and previous iteration
		HashMap<Integer, InOutSet> currentSets = this.inOutSets.get(iterationCount);
		HashMap<Integer, InOutSet> previousSets = this.inOutSets.get(iterationCount - 1);
		
		int currentLine = this.lineCount;
		
		// For each line check that the in/out set coincide between iterations
		while(currentLine > 0) {
			
			InOutSet currInOut = currentSets.get(currentLine);
			InOutSet previousInOut = previousSets.get(currentLine);
			
			if(!currInOut.getInSet().equals(previousInOut.getInSet())) return false;
			if(!currInOut.getOutSet().equals(previousInOut.getOutSet())) return false;
			
			currentLine--;
		}
		
		return true;
	}
	
	/**
	 * Populates the current iteration with in/out sets. Uses empty sets for the first iteration
	 * and copies the previous iteration for iterationCount > 0.
	 * 
	 * @param iterationCount the current iteration number
	 */
	private void populateInOutSet(int iterationCount) {
		
		// Populate with empty in/out sets
		if(iterationCount == 0) {
			
			for(int i = 1; i <= this.lineCount; i++) {
				this.inOutSets.get(iterationCount).put(i, new InOutSet());
			}
		// Carry over previous iteration
		} else {

			// For each line, clone the in/out set object
			for(int i = 1; i <= this.lineCount; i++) {
				
				InOutSet inOut = this.inOutSets.get(iterationCount - 1).get(i);
				HashSet<String> inSet = (HashSet<String>) inOut.getInSet().clone();
				HashSet<String> outSet = (HashSet<String>) inOut.getOutSet().clone();
				
				InOutSet inOutCopy = new InOutSet();
				inOutCopy.setInSet(inSet);
				inOutCopy.setOutSet(outSet);
				
				this.inOutSets.get(iterationCount).put(i, inOutCopy);
			}
		}
	}

	/**
	 * Builds the in set for the current line/iteration combo using the
	 * current function. in[n] = use[n] U (out[n] - def[n])
	 * 
	 * @param functionName the function name
	 * @param iterationCount the current iteration number
	 * @param lineNumber the current line number
	 * @return the in set
	 */
	private HashSet<String> buildInSet(String functionName, int iterationCount, int lineNumber) {
		
		HashSet<String> inSet = new HashSet<String>();
		
		// else / endwhile / endif do not have use/def sets
		if(!this.functionSets.get(functionName).containsKey(lineNumber)) return inSet;
		
		// Get use/def for this function/line
		UseDefSet useDef = this.functionSets.get(functionName).get(lineNumber);
		
		// out[n] - def[n]
		HashSet<String> outDefDiff = (HashSet<String>) this.inOutSets.get(iterationCount).get(lineNumber).getOutSet().clone();
		outDefDiff.removeAll(useDef.getDefSet());
		
		// Add use[n] U (out[n] - def[n])
		inSet.addAll(outDefDiff);
		inSet.addAll(useDef.getUseSet());
		
		return inSet;
	}
	
	/**
	 * Builds the out set for the current line/iteration combo.
	 * out[n] = U of all in[succ of n]
	 * 
	 * @param iterationCount the current iteration number
	 * @param lineNumber the current line number
	 * @return the out set
	 */
	private HashSet<String> buildOutSet(int iterationCount, int lineNumber) {
		
		HashSet<String> outSet = new HashSet<String>();
		
		// For each successor of this node
		for(Integer num : this.successorList.get(lineNumber)) {
			
			// out[n] = U of all in[succ of n]
			if(this.inOutSets.get(iterationCount).containsKey(num)) {
				outSet.addAll(this.inOutSets.get(iterationCount).get(num).getInSet());
			}
		}

		return outSet;
	}
	
	/**
	 * Opens a file for reading.
	 * 
	 * @param filepath the file path to open
	 * @return the reader object for the file
	 */
	private BufferedReader openFileToRead(String filepath) throws FileNotFoundException {
		
		File file = new File(this.filepath);
		return new BufferedReader(new FileReader(file));
	}
	
	/**
	 * Builds the successor list for each function line.
	 * 
	 * @param reader the reader for reading lines
	 */
	private void buildSuccessorList(BufferedReader reader) throws IOException {
		
		String line;
		int lineNumber = 1;
		while((line = reader.readLine()) != null) {
			
			this.successorList.put(lineNumber, new HashSet<Integer>());
			
			// Check for branch start
			String cond = this.checkConditionStart(line);
			if(!cond.equals("")) {
				this.addBranchSuccessor(cond, lineNumber);
			// Check for else statement
			} else if(line.equals(elseStart)) {
				lineNumber++;
				continue;
			// Check for branch end
			} else {
				cond = this.checkConditionEnd(line);
				if(!cond.equals("")) {
					this.branchStack.pop();
					lineNumber++;
					continue;
				}
			}

			String nextLine = this.getLine(lineNumber + 1);
			if(this.checkNextLineBranch(nextLine, lineNumber)) {
				lineNumber++;
				continue;
			}
			
			this.successorList.get(lineNumber).add(lineNumber + 1);
			lineNumber++;
		}
		
		reader.close();
	}
	
	/**
	 * Checks if the line specified is a conditional branch, and if so,
	 * adds to the successor list the branch end lines.
	 * 
	 * @param line the current line being parsed
	 * @param lineNumber the current line number
	 * @return whether the next line was a branch
	 */
	private boolean checkNextLineBranch(String line, int lineNumber) {
		
		if(line.equals(elseStart)) {
			int ifEndLine = this.functionBranches.get(this.branchStack.peek()).getIfEndLine();
			this.successorList.get(lineNumber).add(ifEndLine + 1); // Skip to after endif
			return true;
		} else if(line.equals(whileEnd)) {
			int whileLine = this.functionBranches.get(this.branchStack.peek()).getWhileLine();
			this.successorList.get(lineNumber).add(whileLine);
			this.successorList.get(lineNumber).add(lineNumber + 2); // Skip endwhile
			return true;
		} else if(line.equals(ifEnd)) {
			this.successorList.get(lineNumber).add(lineNumber + 2); // Skip endif
			return true;
		}
		
		return false;
	}
	
	/**
	 * Handles adding successor lines for conditional branch starting lines.
	 * 
	 * @param cond the type of branch
	 * @param lineNumber the current line number
	 */
	private void addBranchSuccessor(String cond, int lineNumber) {
		
		String key = this.buildCondKey(cond, lineNumber);
		
		if(!this.functionBranches.containsKey(key)) return;
		
		this.branchStack.push(key);
		
		// If statement successors
		if(cond.equals("if")) {
			
			// No else
			if(this.functionBranches.get(key).getElseLine() == 0) {
				this.successorList.get(lineNumber).add(lineNumber + 1);
				this.successorList.get(lineNumber).add(this.functionBranches.get(key).getIfEndLine() + 1);
			// If/Else statement
			} else {
				this.successorList.get(lineNumber).add(lineNumber + 1);
				this.successorList.get(lineNumber).add(this.functionBranches.get(key).getElseLine() + 1);
			}
			
		// While statement successors
		} else {
			this.successorList.get(lineNumber).add(lineNumber + 1);
			this.successorList.get(lineNumber).add(this.functionBranches.get(key).getWhileEndLine() + 1);
		}
	}
	
	/**
	 * Reads the HIR file sequentially and stores branch start/end lines
	 * for later usage in the successor. Also sets the total number of lines.
	 * 
	 * @param reader the reader for reading lines
	 */
	private void locateBranches(BufferedReader reader) throws IOException {

		String line;
		int lineNumber = 1;
		while((line = reader.readLine()) != null) {
			
			// Check for branch start
			String cond = this.checkConditionStart(line);
			if(!cond.equals("")) this.createNewBranch(cond, lineNumber);
			
			// Check for else statement
			if(line.equals(elseStart)) this.updateElseLine(lineNumber);
			
			// Check for branch end
			cond = this.checkConditionEnd(line);
			if(!cond.equals("")) this.closeBranch(cond, lineNumber);
			
			lineNumber++;
		}
		
		this.lineCount = lineNumber - 1;
		reader.close();
	}

	/**
	 * Checks if the current line contains a branch start
	 * and if so returns its type.
	 * 
	 * @param line the current line being parsed
	 * @return the type of branch found, "" if none
	 */
	private String checkConditionStart(String line) {
		
		if(line.contains(ifStart) && !line.equals(ifEnd)) return "if";
		if(line.contains(whileStart) && !line.equals(whileEnd)) return "while";
		
		return "";
	}
	
	/**
	 * Checks if the current line contains a branch end
	 * and if so returns its type.
	 * 
	 * @param line the current line being parsed
	 * @return the type of branch found, "" if none
	 */
	private String checkConditionEnd(String line) {
		
		if(line.equals(ifEnd)) return "endif";
		if(line.equals(whileEnd)) return "endwhile";
		
		return "";
	}
	
	/**
	 * Creates a new branch info object by using the type of
	 * branch and line number as key. Also sets the starting
	 * line for the branch. Pushes the branch to the stack
	 * so it's used for later branch keywords (else/endif/endwhile).
	 * 
	 * @param cond the type of branch
	 * @param lineNumber the current line number
	 */
	private void createNewBranch(String cond, int lineNumber) {
		
		String key = this.buildCondKey(cond, lineNumber);
		ConditionalInfo condInfo = new ConditionalInfo();
		
		// Set starting line for the branch
		if(cond.equals("if")) {
			condInfo.setIfLine(lineNumber);
		} else {
			condInfo.setWhileLine(lineNumber);
		}
		
		this.functionBranches.put(key, condInfo);
		this.branchStack.push(key); // Update current branch being handled
	}
	
	/**
	 * Closes a branch info object by setting the ending
	 * line for the branch. Removes the branch from the stack.
	 * 
	 * @param cond the type of branch
	 * @param lineNumber the current line number
	 */
	private void closeBranch(String cond, int lineNumber) {
		
		String key = this.branchStack.pop();
		
		// Set ending line for the branch
		if(cond.equals("endif")) {
			this.functionBranches.get(key).setIfEndLine(lineNumber);
		} else {
			this.functionBranches.get(key).setWhileEndLine(lineNumber);
		}
	}

	/**
	 * Updates a branch info object with the else
	 * line number. Uses the current stack branch
	 * as key.
	 * 
	 * @param lineNumber the current line number
	 */
	private void updateElseLine(int lineNumber) {
		
		String key = this.branchStack.peek();
		this.functionBranches.get(key).setElseLine(lineNumber);
	}
	
	/**
	 * Builds a key to the map of branch info objects.
	 * 
	 * @param cond the type of branch
	 * @param lineNumber the current line number
	 * @return the key built
	 */
	private String buildCondKey(String cond, int lineNumber) {
		return cond + lineNumber;
	}
	
	/**
	 * Fetches a single line identified by its line number.
	 * Should not be used for large files.
	 * 
	 * @param lineNumber the line number to fetch, starting at 1
	 * @return the line fetched, "" if out of range
	 */
	private String getLine(int lineNumber) throws IOException {
		
		List<String> lines = Files.readAllLines(Paths.get(this.filepath));
		if(lineNumber > lines.size()) {
			return "";
		} else return lines.get(lineNumber - 1);
	}
}