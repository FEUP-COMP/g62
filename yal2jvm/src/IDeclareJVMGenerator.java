import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IDeclareJVMGenerator extends DeclareJVMGenerator {

	public IDeclareJVMGenerator(Function function, String moduleName) {
		super(function, moduleName);
	}
	
	public void writeStaticDeclaration(String[] args) {
		if (args.length == 1) {
			JasminGenerator.write(getStaticDeclaration(args[0]));
		} else {
			JasminGenerator.write(getStaticDeclaration(args[0], Integer.parseInt(args[1])));
		}
	}
	
	public String getStaticDeclaration(String name, int value) {
		return ".field static " + name + " I = " + value + "\n";
	}

	public String getStaticDeclaration(String name) {
		return ".field static " + name + " I" + "\n";
	}
	
	@Override
	public void gen(String name) {
		
		String patternString = ".*?\\((.*)\\)";
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(name);
		matcher.find();
		
		String[] args = matcher.group(1).split("( )*;( )*");
		
		loadValue(args[1],"I");
		storeValue(args[0]);
		super.gen(name);
	}

}