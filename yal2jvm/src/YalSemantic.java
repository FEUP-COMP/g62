import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;

public class YalSemantic {

	private static final String fieldSeparator = ";";
	private String moduleName;
	
	// Semantic analysis state variables
	private enum SemanticState {FUNCTION, ASSIGN_LHS, ASSIGN_RHS, CALL, WHILE, IF};
	private static final ArrayList<Element.ElemType> scalarTypes = new ArrayList<Element.ElemType>(Arrays.asList(Element.ElemType.SCALAR, Element.ElemType.CONST));
	private static final ArrayList<SemanticState> exprStates = new ArrayList<SemanticState>(Arrays.asList(SemanticState.WHILE, SemanticState.IF));
	
	private SemanticState state;
	private SemanticState previousState;
	private String currFunctionName;
	private int termCount;
	private Element.ElemType lhsType = Element.ElemType.VOID;
	private Element.ElemType op1Type = Element.ElemType.VOID;
	private Element.ElemType op2Type = Element.ElemType.VOID;
	private String lhsID = "";
	private String op1ID = "";
	private String op2ID = "";
	private boolean previousNodeIndex = false;
	private ArrayList<String> callArgs = new ArrayList<String>();
	private ArrayList<HashSet<String>> localsStack = new ArrayList<HashSet<String>>();
	private int stackIndex;
	private Deque<Integer> whileStackIndex = new ArrayDeque<Integer>();
	
	/**
	 * Constructs a semantic analyser object to run the semantic analisys of a .yal file.
	 */
	public YalSemantic() {
		this.moduleName = YalParser.moduleName;
	}

	/**
	 * Runs semantic analysis on each function of a specific .yal file.
	 */
	public void runAnalyser() {

		LinkedHashMap<String, SimpleNode> functions = YalParser.functionsAST;
		
		// Validate the semantics of each function
		for(Map.Entry<String, SimpleNode> entry : functions.entrySet()) {

			// Initialize variables for each function
			this.state = SemanticState.FUNCTION;
			this.currFunctionName = entry.getKey();
			this.localsStack.add(new HashSet<String>());
			this.stackIndex = 0;
			this.recurseFunction(entry.getValue());
			this.localsStack = new ArrayList<HashSet<String>>();
			this.whileStackIndex = new ArrayDeque<Integer>();
		}
	}
	
	/**
	 * Recurses through a function's nodes to perform semantic analysis.
	 * 
	 * @param node the current node being visited
	 */
	private void recurseFunction(SimpleNode node) {
		
		String nodeName = yal2jvmTreeConstants.jjtNodeName[node.getId()].toLowerCase();
		this.parseNode(node);
		
		for(int i = 0; i < node.jjtGetNumChildren(); i++) {
			recurseFunction((SimpleNode) node.jjtGetChild(i));
		}
		
		if(nodeName.equals("call")) this.callFinish(node);
		else if(nodeName.equals("assign")) this.assignFinish(node);
		else if(nodeName.equals("if")) this.ifSetFinish(node);
		else if(nodeName.equals("while")) this.whileSetFinish(node);
	}
	
	/**
	 * Parses a function node in search of semantic errors.
	 * 
	 * @param node the current node being visited
	 */
	private void parseNode(SimpleNode node) {
		
		String nodeName = yal2jvmTreeConstants.jjtNodeName[node.getId()].toLowerCase();
		String nodeValue = "";

		// Assign AST value if parsing a leaf
		if(node.jjtGetNumChildren() == 0 && !nodeName.equals("stmtlst")) {
			nodeValue = ((String) node.jjtGetValue()).toLowerCase();
		}

		switch(nodeName) {

		case "assign":
			this.state = SemanticState.ASSIGN_LHS;
			break;
			
		case "rhs":
			if(this.state.equals(SemanticState.ASSIGN_LHS)) this.state = SemanticState.ASSIGN_RHS;
			break;
			
		case "term":
			this.termCount++;
			break;
			
		case "call":
			if(!this.state.equals(SemanticState.FUNCTION)) this.previousState = this.state;
			this.state = SemanticState.CALL;
			break;
			
		case "size":
			
			// LHS .size access error
			if(this.state.equals(SemanticState.ASSIGN_LHS)) {
				
				String errMsg = ".size cannot be applied to the left-hand side";
				this.addToErrors(node, errMsg);

			// RHS .size on non arrays error
			} else if(this.termCount == 1) {
				this.op1Type = Element.ElemType.SCALAR;
				if(this.getVarType(this.currFunctionName, this.op1ID).equals(Element.ElemType.SCALAR)) {
					String errMsg = ".size can only be applied to ARRAY type";
					this.addToErrors(node, errMsg);
				}
			} else if(this.termCount == 2) {
				this.op2Type = Element.ElemType.SCALAR;
				if(this.getVarType(this.currFunctionName, this.op2ID).equals(Element.ElemType.SCALAR)) {
					String errMsg = ".size can only be applied to ARRAY type";
					this.addToErrors(node, errMsg);
				}
			}
			break;
			
		case "index":
			if(this.state.equals(SemanticState.ASSIGN_LHS)) {
				if(this.getVarType(this.currFunctionName, this.lhsID).equals(Element.ElemType.VOID)) {
					String errMsg = "undeclared variable \"" + this.lhsID + "\"";
					this.addToErrors(node, errMsg);
				}
				
				this.previousNodeIndex = true;
			}
			break;
			
		case "id":

			// Check if index was declared
			if(this.previousNodeIndex) {
				if(this.getVarType(this.currFunctionName, nodeValue).equals(Element.ElemType.VOID)) {
					String errMsg = "undeclared variable used as index \"" + nodeValue + "\"";
					this.addToErrors(node, errMsg);
				// If local variable check that it reaches this point initialized
				} else if(!this.isLocalInitialized(this.currFunctionName, nodeValue)) {
					String errMsg = "variable \"" + nodeValue + "\" might reach this point uninitialized";
					this.addToErrors(node, errMsg);
				}
			}
			
			// LHS assignment variable type
			if(this.state.equals(SemanticState.ASSIGN_LHS)) {
				this.lhsType = this.getVarType(this.currFunctionName, nodeValue);
				this.lhsID = nodeValue;
				if(!this.previousNodeIndex) this.localsStack.get(this.stackIndex).add(nodeValue);
				
			// RHS assignment variable types
			} else if(this.state.equals(SemanticState.ASSIGN_RHS) || YalSemantic.exprStates.contains(this.state)) {
				
				// Check that variable was declared
				boolean undeclared = false;
				if(this.getVarType(this.currFunctionName, nodeValue).equals(Element.ElemType.VOID)) {
					String errMsg = "undeclared variable \"" + nodeValue + "\"";
					this.addToErrors(node, errMsg);
					undeclared = true;
				// If local variable check that it reaches this point initialized
				} else if(!this.isLocalInitialized(this.currFunctionName, nodeValue)) {
					String errMsg = "variable \"" + nodeValue + "\" might reach this point uninitialized";
					this.addToErrors(node, errMsg);
				}
				
				if(this.termCount == 1) {
					this.op1Type = undeclared ? Element.ElemType.SCALAR : this.getVarType(this.currFunctionName, nodeValue);
					this.op1ID = nodeValue;
				} else if(this.termCount == 2) {
					this.op2Type = undeclared ? Element.ElemType.SCALAR : this.getVarType(this.currFunctionName, nodeValue);
					this.op2ID = nodeValue;
				}
			} else if(this.state.equals(SemanticState.CALL)) {
				this.callArgs.add(nodeValue);
			}
			
			break;

		case "arraysize":
			
			if(!this.getVarType(this.currFunctionName, this.lhsID).equals(Element.ElemType.ARRAY)) {
				String errMsg = "variable \"" + this.lhsID + "\" has already been declared as scalar";
				this.addToErrors(node, errMsg);
			}
			break;
			
		case "stmtlst":
			if(YalSemantic.exprStates.contains(this.state)) {
				this.resetParseVars();
			}
			break;
			
		case "while":
			
			this.state = SemanticState.WHILE;
			
			// Create duplicate of the outer set
			HashSet<String> whileCopy = (HashSet<String>) this.localsStack.get(this.stackIndex).clone();
			this.localsStack.add(whileCopy);
			this.whileStackIndex.push(this.stackIndex); // Push outer set stack index
			this.stackIndex = this.localsStack.size() - 1; // Point to while stack
			break;
			
		case "if":
			
			this.state = SemanticState.IF;
			
			// Create 2 duplicates of the outer set
			HashSet<String> ifCopy = (HashSet<String>) this.localsStack.get(this.stackIndex).clone();
			HashSet<String> elseCopy = (HashSet<String>) this.localsStack.get(this.stackIndex).clone();
			this.localsStack.add(ifCopy);
			this.localsStack.add(elseCopy);
			this.stackIndex = this.localsStack.size() - 2; // Point to ifbody stack
			break;
			
		case "elsebody":
			this.stackIndex++; // Point to elsebody stack
			break;
			
		case "argumentlist":
			this.callArgs.add(YalSemantic.fieldSeparator);
			break;
			
		default:
			
			if(node.jjtGetNumChildren() != 0) break;
			
			// Parse leaf if it contains a numeric value
			if(YalUtility.getInstance().isInteger(nodeValue)) {
				
				this.previousNodeIndex = false;
				
				// Constants on rhs of assignments
				if(this.state.equals(SemanticState.ASSIGN_RHS)) {
					if(this.termCount == 1) {
						this.op1Type = Element.ElemType.CONST;
					} else if(this.termCount == 2) {
						this.op2Type = Element.ElemType.CONST;
					}
				} else if(this.state.equals(SemanticState.CALL)) {
					this.callArgs.add(nodeValue);
				}
			}
			break;
			
		}
	}

	/**
	 * Uses the VariableFinder class to find the type of a given variable
	 * by checking the current function locals / params and the module globals.
	 * 
	 * @param functionName name of the function to check
	 * @param varName the variable name
	 * @return the enumerator representing the variable type
	 */
	private Element.ElemType getVarType(String functionName, String varName) {
		
		VariableFinder varFinder = new VariableFinder(YalParser.functionTables.get(functionName));
		return varFinder.lookupVarType(varName);	
	}

	/**
	 * Checks if a local variable is initialized according to the locals stack.
	 * 
	 * @param functionName name of the function locals to check
	 * @param varName the local variable name
	 * @return whether the local variable is initialized
	 */
	private boolean isLocalInitialized(String functionName, String varName) {
		
		// Stop if variable isn't local
		VariableFinder varFinder = new VariableFinder(YalParser.functionTables.get(functionName));
		if(!varFinder.isLocal(varName)) return true;
		
		return this.localsStack.get(this.stackIndex).contains(varName);
	}
	
	/**
	 * Intersects the initialized variables sets from a if/else statement.
	 * Points the index to the outer set of the if/else statement and removes
	 * the if/else sets from the stack.
	 * 
	 * @param node the current node being visited
	 */
	private void ifSetFinish(SimpleNode node) {
		
		HashSet<String> ifStack = this.localsStack.get(this.localsStack.size() - 2);
		HashSet<String> elseStack = this.localsStack.get(this.localsStack.size() - 1);

		// Point to outer set
		this.stackIndex = this.stackIndex - 2;
		if(this.stackIndex < 0) this.stackIndex = 0;
		
		// Pop 2 stacks
		this.localsStack.remove(this.localsStack.size() - 1);
		this.localsStack.remove(this.localsStack.size() - 1);

		// Intersect if/else sets
		ifStack.retainAll(elseStack);
		this.localsStack.set(this.stackIndex, (HashSet<String>) ifStack.clone());
	}
	
	/**
	 * Removes the while set from the stack and points the index to
	 * the outer set of the while statement, found by popping the
	 * while index stack.
	 * 
	 * @param node the current node being visited
	 */
	private void whileSetFinish(SimpleNode node) {
		
		this.stackIndex = this.whileStackIndex.pop();
		this.localsStack.remove(this.localsStack.size() - 1);
	}

	/**
	 * Checks for assignment semantic errors and adds them
	 * to the error list if found.
	 * 
	 * @param node the current node being visited
	 */
	private void assignFinish(SimpleNode node) {

		// Scalar assignment to 1 or 2 invalid operands
		if((this.lhsType.equals(Element.ElemType.SCALAR) || this.lhsType.equals(Element.ElemType.ARRAY)) && this.termCount == 2 &&
				(!YalSemantic.scalarTypes.contains(this.op1Type) || !YalSemantic.scalarTypes.contains(this.op2Type))) {

			String errMsg = "operation not defined for the argument types " + this.op1Type + " " + this.op2Type;
			this.addToErrors(node, errMsg);
		
		// Scalar assignment to 1 invalid operand
		} else if(this.lhsType.equals(Element.ElemType.SCALAR) && this.termCount == 1 &&
				(!YalSemantic.scalarTypes.contains(this.op1Type))) {
			
			String errMsg = this.lhsType + " assignment to incompatible type " + this.op1Type;
			this.addToErrors(node, errMsg);
		}
		
		this.resetParseVars();
	}

	/**
	 * Call() node finished, look for semantic error in function call.
	 * 
	 * @param node the current node being visited
	 */
	private void callFinish(SimpleNode node) {
		
		// Reset state to previous state
		if(this.previousState != null) {
			this.state = this.previousState;
			this.previousState = null;
		}

		// Find id/args separator
		int separatorI = this.callArgs.indexOf(YalSemantic.fieldSeparator);
		int iterateTo = (separatorI < 0) ? this.callArgs.size() : separatorI;

		// Build function id
		ArrayList<String> functionID = new ArrayList<String>();
		if(iterateTo == 1) {
			functionID.add(this.moduleName);
			functionID.add(this.callArgs.get(0));
		} else {
			for(int i = 0; i < iterateTo; i++) {
				functionID.add(this.callArgs.get(i));
			}
		}

		// Build argument list
		ArrayList<String> funcArgs = new ArrayList<String>();
		if(separatorI >= 0) {
			for(int i = separatorI + 1; i < this.callArgs.size(); i++) {
				funcArgs.add(this.callArgs.get(i));
			}
		}
		
		// Check if internal function
		if(functionID.get(0).equals(this.moduleName)) {
			
			// Error if function hasn't been declared
			if(!YalParser.functionTables.containsKey(functionID.get(1))) {
				String msg = "call to undefined internal function - \"" + functionID.get(1) + "()\"";
				this.addToErrors(node, msg);
			// Function exists, assign return type to current operand
			} else {
				
				Function func = YalParser.functionTables.get(functionID.get(1));
				if(func.getReturnElem() != null) {
					if(this.termCount == 1) {
						
						// Assigning array to return reference
						if(this.lhsType.equals(Element.ElemType.ARRAY)) {
							Element.ElemType returnType = func.getReturnElem().parseAsElemType();
							if(!returnType.equals(Element.ElemType.ARRAY)) {
								String msg = this.lhsType + " assignment to incompatible type " + returnType;
								this.addToErrors(node, msg);
							}
						}

						this.op1Type = func.getReturnElem().parseAsElemType();

					// Binary operations
					} else if(this.termCount == 2) {
						this.op2Type = func.getReturnElem().parseAsElemType();
					}
				} else {
					if(this.termCount == 1) {
						this.op1Type = Element.ElemType.VOID;
					} else if(this.termCount == 2) {
						this.op2Type = Element.ElemType.VOID;
					}
				}
				
				// Validate number of parameters passed to internal function
				if(funcArgs.size() != func.getParams().size()) {
					String msg = "wrong number of arguments for internal function \"" + functionID.get(1) + "()\"";
					this.addToErrors(node, msg);
				// Validate type of parameters passed to internal function
				} else {
				
					int i = 0;
					for(Map.Entry<String, Element> entry : func.getParams().entrySet()) {
						
						Element.ElemType argGivenType;
						if(YalUtility.getInstance().isInteger(funcArgs.get(i))) {
							argGivenType = Element.ElemType.SCALAR;
						} else {
							argGivenType = this.getVarType(this.currFunctionName, funcArgs.get(i));
						}

						// Type mismatch of parameter passed to internal function call
						if(!argGivenType.equals(Element.ElemType.VOID)) {
							if(!entry.getValue().parseAsElemType().equals(argGivenType)) {
								String msg = "\"" + funcArgs.get(i) + "\"'s type is not valid for internal function call \"" + functionID.get(1) + "()\"";
								this.addToErrors(node, msg);
							// If local variable check that it reaches this point initialized
							} else if(!this.isLocalInitialized(this.currFunctionName, funcArgs.get(i))) {
								String errMsg = "variable \"" + funcArgs.get(i) + "\" might reach this point uninitialized";
								this.addToErrors(node, errMsg);
							}
						// Variable not declared
						} else {
							String msg = "variable \"" + funcArgs.get(i) + "\" undeclared for internal function call \"" + functionID.get(1) + "()\"";
							this.addToErrors(node, msg);
						}
						
						i++;
					}

				}
			}
	
		// External function
		} else {
			
			// Check passed arguments have been declared and initialized
			for(String varName : funcArgs) {
			
				// Check that variable was declared
				if(this.getVarType(this.currFunctionName, varName).equals(Element.ElemType.VOID)) {
					String errMsg = "undeclared variable \"" + varName + "\"";
					this.addToErrors(node, errMsg);
				// If local variable check that it reaches this point initialized
				} else if(!this.isLocalInitialized(this.currFunctionName, varName)) {
					String errMsg = "variable \"" + varName + "\" might reach this point uninitialized";
					this.addToErrors(node, errMsg);
				}
			}
			
			// Assume CONST return so it doesn't activate type mismatch errors
			if(this.termCount == 1) {
				this.op1Type = Element.ElemType.CONST;
			} else if(this.termCount == 2) {
				this.op2Type = Element.ElemType.CONST;
			}
		}
		
		this.callArgs = new ArrayList<String>();
	}

	/**
	 * Resets parser variables that affect parsing decisions.
	 */
	private void resetParseVars() {
		
		this.state = SemanticState.FUNCTION;
		this.termCount = 0;
	    this.lhsType = Element.ElemType.VOID;
		this.op1Type = Element.ElemType.VOID;
		this.op2Type = Element.ElemType.VOID;
		this.op1ID = "";
		this.op2ID = "";
		this.lhsID = "";
		this.previousNodeIndex = false;
		this.callArgs = new ArrayList<String>();
	}
	
	/**
	 * Adds an error to the error list for later printing.
	 * 
	 * @param node the current node being visited
	 * @param message the error message
	 */
	private void addToErrors(SimpleNode node, String message) {

		String errMsg = "Line " + node.getLineNumber() + ": " + message;
		YalParser.errorMessages.put(YalParser.semanticErrCount, errMsg);
		YalParser.semanticErrCount++;
	}
}
