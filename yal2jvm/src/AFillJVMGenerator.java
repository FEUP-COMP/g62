import java.util.regex.Matcher; 
import java.util.regex.Pattern;

public class AFillJVMGenerator extends JVMGenerator {
	
	private int loopNumber;
	private int registerAux;

	public AFillJVMGenerator(Function function, String moduleName, int loopNumber, int registerAux) {
		super(function, moduleName);
		this.loopNumber = loopNumber;
		this.registerAux = registerAux;
	}
	
	@Override
	public void gen(String name) {
		
		String patternString = ".*?\\((.*)\\)";
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(name);
		matcher.find();
		
		String[] args = matcher.group(1).split("( )*;( )*");
		
		String reference = args[0];
		String fill = args[1];
		String size = reference + ".size";
		
		loadValue("0");
		storeScalarVar(registerAux);
		JasminGenerator.write("\n");
		
		JasminGenerator.write("loop" + loopNumber + ":\n\n");
		
		loadScalarVar(registerAux);
		loadValue(size);
		JasminGenerator.write("if_icmpge loop" + loopNumber + "_end\n\n");
		
		loadValue(reference);
		loadScalarVar(registerAux);
		loadValue(fill);
		JasminGenerator.write("iastore\n\n");
		
		JasminGenerator.write("iinc " + registerAux + " 1\n");
		JasminGenerator.write("goto loop" + loopNumber + "\n\n");
		
		JasminGenerator.write("loop" + loopNumber + "_end:\n\n");	
	}

}
