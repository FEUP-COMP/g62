import java.util.HashSet;

public class UseDefSet {

	private HashSet<String> useSet = new HashSet<String>();
	private HashSet<String> defSet = new HashSet<String>();
	
	/**
	 * @return the used variables set
	 */
	public HashSet<String> getUseSet() {
		return useSet;
	}
	/**
	 * @param useSet the used variables set to set
	 */
	public void setUseSet(HashSet<String> useSet) {
		this.useSet = useSet;
	}
	/**
	 * @return the defined variables set
	 */
	public HashSet<String> getDefSet() {
		return defSet;
	}
	/**
	 * @param defSet the defined variables set to set
	 */
	public void setDefSet(HashSet<String> defSet) {
		this.defSet = defSet;
	}
}