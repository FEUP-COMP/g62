import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class YalParser {

	private static final int maxErrors = 10;
	
	// Command line parameters
	private static boolean optimizeFlag = false;
	private static boolean localVarFlag = false;
	private static int localVarCount = 0;

	// Syntax / Semantic helper variables
	public static int errorCount = 0;
	public static boolean invalidStmtlst = false;
	public static int semanticErrCount = 0;
	public static LinkedHashMap<Integer, String> errorMessages = new LinkedHashMap<Integer, String>();

	// yal file information
	private static InputStream fileStream;
	public static String moduleName;
	public static String yalFilename;
	static String yalFilenameNoExt;

	// Symbol tables
	public static LinkedHashMap<String, Element> globalsTable = new LinkedHashMap<String, Element >();
	public static LinkedHashMap<String, Function> functionTables = new LinkedHashMap<String, Function>();

	// Subdivided AST, functions and global declarations
	public static ArrayList<SimpleNode> globalsAST = new ArrayList<SimpleNode>();
	public static LinkedHashMap<String, SimpleNode> functionsAST = new LinkedHashMap <String, SimpleNode>();

	// File paths
	public static String hirFilesPath = "../hirFiles";
	public static String jasminFilesPath = "../jasminFiles";

	// File extensions
	public static String hirFileExt = "bla";
	public static String jasminFileExt = "j";

	public static void run(String args[]) throws ParseException, IOException {

		// Wrong argument number message
		if(args.length < 1) {
			System.out.println("Usage: yal2jvm [-r=<num>] [-o] [-d] <input_file.yal>");
			return;
		}
		
		// Validate .yal extension
		if(!args[args.length - 1].toLowerCase().matches(".*\\.yal")) {
			System.out.println("File provided does not have .yal extension!");
			System.exit(1);
		}
		
		// Create parser from file stream
		try	{
			fileStream = new FileInputStream(args[args.length - 1]);
			File yalFile = new File(args[args.length - 1]);
			YalParser.yalFilename = yalFile.getName();
			YalParser.yalFilenameNoExt = YalParser.yalFilename.substring(0, YalParser.yalFilename.lastIndexOf('.'));
		} catch(FileNotFoundException e) {
			System.out.println("File specified could not be found!");
			return;
		}

		new yal2jvm(fileStream);

		// Parse additional command line arguments
		if(args.length > 1) parseCmdArguments(args);
		YalDebug.getInstance().printFlagStatus(localVarFlag, optimizeFlag, localVarCount);
		
		// Run parser
		SimpleNode root = yal2jvm.Module();

		// Syntax error automated testing, only runs if YalDebug.syntaxDebugExit flag is true
		YalDebug.getInstance().syntaxDebugExit(YalParser.getNthPositiveInt(args[args.length - 1], 0));

		// Exit on syntactic errors
		if(YalParser.errorCount > 0) {
			System.out.println(YalParser.errorCount + " syntax errors found and reported, aborting compilation");
			System.exit(1);
		}
		
		// Run HIR generator
		HIR genIR = new HIR();
		
		HashMap<String, HashMap<Integer, UseDefSet>> functionUseDefSets = null;
		try {
			functionUseDefSets = genIR.genIRAndSymbol();
		} catch(IOException e) {
			System.out.println("Exception creating HIR and/or symbol table: " + e.getMessage());
			System.exit(1);
		} catch(Exception e) {
			System.out.println("Exception converting to HIR instructions!");
			e.printStackTrace();
			System.exit(1);
		}
		
		// Run semantic analysis
		YalSemantic semantic = new YalSemantic();
		semantic.runAnalyser();
		
		// Print semantic errors and exit
		if(YalParser.errorMessages.size() != 0) {
			YalParser.printSemanticErrors();
			System.exit(1);
		}
		
		// Run register allocation if option specified
		if(YalParser.localVarFlag) {
			
			RegisterAllocation reg = new RegisterAllocation(functionUseDefSets, YalParser.localVarCount);
			if(!reg.runAllocation()) {
				System.out.println("Could not allocate all locals, aborting compilation...");
				return;
			}
		}
		
		// Run Jasmin generator
		try {
			JasminGenerator.gen(root);
		} catch(IOException e) {
			System.out.println("Exception creating Jasmin file: " + e.getMessage());
			System.exit(1);
		} catch(Exception e) {
			System.out.println("Exception converting to Jasmin instructions!");
			e.printStackTrace();
			System.exit(1);
		}
		
		System.out.println("yal file parsed successfully");
	}

	/**
	 * Creates a path to a compiler resource specified by the parameters.
	 * 
	 * @param functionName the function name to create the path for, can be null
	 * @param folderPath the folder path of the resources
	 * @param fileExt the file extension of the resources
	 * @return the file path to the requested resource
	 */
	public static String buildFilePath(String functionName, String folderPath, String fileExt) {
		
		String pathBuild = folderPath + "/" + YalParser.yalFilenameNoExt + "/" + YalParser.moduleName;
		if(functionName != null) pathBuild += "_" + functionName;
		return pathBuild + "." + fileExt;
	}

	/**
	 * Prints semantic errors found up to {@value #maxErrors} errors.
	 */
	private static void printSemanticErrors() {
		
		int errCount = 0;
		System.out.println("Semantic errors found:");
		for(Map.Entry<Integer, String> entry : YalParser.errorMessages.entrySet()) {
			if(errCount == YalParser.maxErrors) break;
			System.out.println("\t" + entry.getValue());
			errCount++;
		}
		
		System.out.println(errCount + " semantic errors found and reported, aborting compilation");
	}

	/**
	 * Parses optional command line arguments and sets respective flags.
	 * 
	 * @param args the command line arguments
	 */
	private static void parseCmdArguments(String[] args) {
		
		for(String str : args) {
			
			// JVM local variable count parsing
			if(str.contains("-r")) {
				int localVar = getNthPositiveInt(str, 0);
				if(localVar > 0 && localVar < 256) {
					localVarFlag = true;
					localVarCount = localVar;
				} else System.out.println("Invalid local variable count passed! Must be value in range [1, 255].");
			} else if(str.equals("-o")) {
				optimizeFlag = true;
			} else if(str.equals("-d")) {
				YalDebug.getInstance().setDebugFlag(true);
			}
		}
	}

	/////////////////////////////////////////////////////////////////////////////
	// Utility functions
	/////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Looks for the nth numeric value on a string.
	 * 
	 * @param str the string to look for numeric values
	 * @param index which numeric value to return
	 * @return the nth numeric value if found, -1 otherwise
	 */
	private static int getNthPositiveInt(String str, int index) {
		
		str = str.replaceAll("[^0-9]+", " ");
		String[] results = str.trim().split(" ");
		
		if(results.length < index) return -1;
		
		if(results [index].length() > 0) {
			return Integer.parseInt(results[index]);
		} else return -1;
	}

	/**
	 * Skips tokens up to specified tokenID and increments error count,
	 * flag determines whether specified token gets consumed.
	 * 
	 * @param tokenID the token to search for
	 * @param consumeLast whether to consume the specified token
	 */
	public static void skipTo(int tokenID, boolean consumeLast) {
		
		Token t;
		while(true) {
			
			t = yal2jvm.getToken(1); // Get next token but don't consume it
			
			// Check if expected token or EOF
			if(t.kind == tokenID || t.kind == yal2jvm.EOF)	{
				if(consumeLast) yal2jvm.getNextToken();
				errorCount++;
				return;
			}
			
			yal2jvm.getNextToken(); // Consume token
		}
	}

	/**
	 * Looks for a specific Token starting from the current stream position.
	 * 
	 * @param tokenID the token to search for
	 * @return -1 on error, stream position of token if found
	 */
	private static int findToken(int tokenID) {
		
		Token t;
		int index = 0;
		
		while(true) {
			
			t = yal2jvm.getToken(index);
			
			if(t.kind == tokenID) {
				return index;
			} else if(t.kind == yal2jvm.EOF) {
				return -1;
			}
			
			index++;
		}
	}

	/**
	 * Finds both specified tokens and compares their relative positions.
	 * 
	 * @param firstToken the first token to search for
	 * @param secondToken the second token to search for
	 * @return positive value if firstToken came first, negative value if not, 0 on error
	 */
	private static int compareTokenPos(int firstToken, int secondToken) {
		
		int firstPos = findToken(firstToken);
		int secondPos = findToken(secondToken);
		
		// Handle cases where one or both tokens couldn't be found
		if(firstPos < 0 && secondPos < 0) return 0;
		else if(firstPos < 0) return -1;
		else if(secondPos < 0) return 1;
		return secondPos - firstPos;
	}

	/////////////////////////////////////////////////////////////////////////////
	// Syntax/Semantic error handling
	/////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Prints error line/column along with error token and a small error reason.
	 * 
	 * @param e the exception thrown by JavaCC
	 * @param info the message to print
	 */
	public static void printError(ParseException e, String info) {
		Token err = yal2jvm.getToken(1);
		System.out.println("Error at [" + err.beginLine + ":" + err.beginColumn + "] on token \"" + err.image + "\" \t- " + info);
	}

	/**
	 * Call() context sensitive error handling.
	 * 
	 * @param e the exception thrown by JavaCC
	 */
	public static void callRuleError(ParseException e) {
		
		System.out.println(e.toString()); // TODO insert standardized error
		
		// Validate Call() statement by comparing ";" position relative to "{" position
		int result = compareTokenPos(yal2jvm.PVIRG, yal2jvm.LCHAVETA);
		
		// Couldn't find ";" or "{", skip to "}" and hope it's there
		if(result == 0) {
			skipTo(yal2jvm.RCHAVETA, false);
		// Found ";" first
		} else if(result > 0) {
			skipTo(yal2jvm.PVIRG, true);
		// Found "{" before ";"
		} else if(result < 0) {
			skipTo(yal2jvm.LCHAVETA, true);
			invalidStmtlst = true; // Call() found but "{" was found next, assume failed While/If/Function
		}
	}

	/**
	 * Assign() context sensitive error handling.
	 * 
	 * @param e the exception thrown by JavaCC
	 */
	public static void assignRuleError(ParseException e) {
		
		System.out.println(e.toString()); // TODO insert standardized error
		
		// Validate Assign() statement by looking for ";", "(" and ")"
		int RPARres = compareTokenPos(yal2jvm.PVIRG, yal2jvm.RPAR);
		int LPARres = compareTokenPos(yal2jvm.RPAR, yal2jvm.LPAR);
		
		// Found a ")" after a ";" but before another "(", this could mean a Call() statement had ";" in it
		if(RPARres > 0 && LPARres > 0) {
			skipTo(yal2jvm.PVIRG, true);
			errorCount--; // Don't want to increment twice
		}
		
		skipTo(yal2jvm.PVIRG, true);
	}

	/**
	 * Stmt() context sensitive error handling.
	 * 
	 * @param e the exception thrown by JavaCC
	 */
	public static void stmtRuleError(ParseException e) {
		System.out.println(e.toString()); // TODO insert standardized error
		skipTo(yal2jvm.RCHAVETA, true); // TODO context sensitive skip
	}
	
	/**
	 * Inserts a new function tree into the function AST map. If the
	 * function is already present, a duplicate function name error
	 * is added to the error list.
	 * 
	 * @param functionName the function name to insert
	 * @param node the current node being visited
	 */
	public static void insertFunctionName(String functionName, SimpleNode node) {
		
		if(YalParser.functionsAST.containsKey(functionName)) {
			String errMsg = "Line " + node.getLineNumber() + ": duplicate function declared - \"" + functionName + "()\"";
			YalParser.errorMessages.put(YalParser.semanticErrCount, errMsg);
			YalParser.semanticErrCount++;
			return;
		}
		
		YalParser.functionsAST.put(functionName, node);
	}
}