public abstract class DeclareJVMGenerator extends JVMGenerator {

	public DeclareJVMGenerator(Function function, String moduleName) {
		super(function, moduleName);
	}
	
	public abstract void writeStaticDeclaration(String[] args);
	
	public abstract String getStaticDeclaration(String name, int value);

	public abstract String getStaticDeclaration(String name);

}
