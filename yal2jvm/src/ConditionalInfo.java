
public class ConditionalInfo {

	private int ifLine = 0;
	private int elseLine = 0;
	private int ifEndLine = 0;
	private int whileLine = 0;
	private int whileEndLine = 0;

	/**
	 * @return the IF starting line
	 */
	public int getIfLine() {
		return ifLine;
	}

	/**
	 * @param ifLine the IF starting line to set
	 */
	public void setIfLine(int ifLine) {
		this.ifLine = ifLine;
	}

	/**
	 * @return the ELSE starting line
	 */
	public int getElseLine() {
		return elseLine;
	}

	/**
	 * @param elseLine the ELSE starting line to set
	 */
	public void setElseLine(int elseLine) {
		this.elseLine = elseLine;
	}

	/**
	 * @return the IF ending line
	 */
	public int getIfEndLine() {
		return ifEndLine;
	}

	/**
	 * @param ifEndLine the IF ending line to set
	 */
	public void setIfEndLine(int ifEndLine) {
		this.ifEndLine = ifEndLine;
	}

	/**
	 * @return the WHILE starting line
	 */
	public int getWhileLine() {
		return whileLine;
	}

	/**
	 * @param whileLine the WHILE starting line to set
	 */
	public void setWhileLine(int whileLine) {
		this.whileLine = whileLine;
	}

	/**
	 * @return the WHILE ending line
	 */
	public int getWhileEndLine() {
		return whileEndLine;
	}

	/**
	 * @param whileEndLine the WHILE ending line to set
	 */
	public void setWhileEndLine(int whileEndLine) {
		this.whileEndLine = whileEndLine;
	}
}
