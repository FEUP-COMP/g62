import java.io.File;

public class YalUtility {
	
	private static YalUtility singleton = new YalUtility();
	
	/**
	 * Private constructor for singleton pattern.
	 */
	private YalUtility() {}
	
	/**
	 * @return the singleton instance of the class
	 */
	public static YalUtility getInstance() {
		return singleton;
	}
	
	/**
	 * Creates directory specified by path if it doesn't already exist.
	 * 
	 * @param dirPath the directory path to create
	 */
	public void createDirIfNotExists(String dirPath) {
		
	    File directory = new File(dirPath);
	    if(!directory.exists()) {
	        directory.mkdir();
	    }
	}
	
	/**
	 * Checks whether a string can be converted to an Integer.
	 * 
	 * @param toCheck the string to check
	 * @return boolean whether the string can be converted to an Integer
	 */
	public boolean isInteger(String toCheck) {

		try {
			Integer.parseInt(toCheck);
		} catch(NumberFormatException e) {  
			return false;
		}

		return true;
	}
}
