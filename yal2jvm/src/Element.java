
public class Element {
	
	// Enum for element types
	enum ElemType {CONST, STRING, ARRAY, SCALAR, VOID};
	
	private String type;
	private String name;
	private boolean initialized = false;
	private int register;
	
	public Element(String name, String type, boolean initialized) {
		this.name = name;
		this.type = type;
		this.initialized = initialized;
		this.register = -1;
	}
	
	public Element(String name, String type) {
		this.name = name;
		this.type = type;
		this.register = -1;
	}
	
	public Element(String name) {
		this.name = name;
		this.register = -1;
	}
	
	public ElemType parseAsElemType() {
		if(this.type.equals("array")) return ElemType.ARRAY;
		else if(this.type.equals("scalar")) return ElemType.SCALAR;
		else return ElemType.VOID;
	}
	
	public void declare(String type) {
		this.type = type;
	}
	
	public void initialize() {
		initialized = true;
	}
	
	public void setInitialize(boolean value) {
		initialized = value;
	}
	
	public String getType() {
		return type;
	}
	
	public String getName() {
		return name;
	}

	public String getUniqueID() {
		return this.name + "_" + this.type;
	}
	
	public static String getUniqueID(String name, String type) {
		return name + "_" + type;
	}
	
	public static String getType(String ID) {
		return ID.split("_")[0];
	}
	
	public static String getName(String ID) {
		return ID.split("_")[1];
	}
	
	public int getRegister() {
		return register;
	}

	public void setRegister(int register) {
		this.register = register;
	}

	@Override
	public String toString() {
		return this.name + " - " + this.type;
	}
}
