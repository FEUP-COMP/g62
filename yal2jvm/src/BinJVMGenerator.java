import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BinJVMGenerator extends JVMGenerator {
	
	private static String[] operators = {"<<",">>",">>>","&","|","^"};
	private static String[] operatorsInstructions = {"ishl","ishr","iushr","iand","ior","ixor"};

	public BinJVMGenerator(Function function, String moduleName) {
		super(function, moduleName);
	}
	
	public void gen(String line) {
		String patternString = ".*?\\((.*)\\)";
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(line);
		matcher.find();
		
		String[] args = matcher.group(1).split("( )*;( )*");
		
		String[] ops = args[1].split(JVMGenerator.termSeparator);
		
		for(int i = 0; i < ops.length; i++) {
			ops[i] = ops[i].replaceAll("\\s+","");
		}
		
		String lhs = args[0];
		
		if(Pattern.matches(".+?\\[(.)+\\]",lhs)) {
			VariableFinder finder = new VariableFinder(this.function);
			int registerAux = finder.getLastUsedRegister();
			
			loadValue(ops[0],"I");
			loadValue(ops[2],"I");
			writeOperation(ops[1]);
			storeScalarVar(registerAux);
			
			storeValue(lhs);
			loadScalarVar(registerAux);
			JasminGenerator.write("iastore");
		} else {
			
			loadValue(ops[0],"I");
			loadValue(ops[2],"I");
			writeOperation(ops[1]);
			storeValue(lhs);
		}
		
		JasminGenerator.write("\n");
	}
	
	public void writeOperation(String operator) {
		int index = Arrays.asList(operators).indexOf(operator);
		
		if(index == -1) {
			return;
		}
		
		String instruction = operatorsInstructions[index];
		
		JasminGenerator.write(instruction + "\n");
	}

}
