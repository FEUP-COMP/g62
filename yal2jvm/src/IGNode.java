import java.util.HashSet;

public class IGNode {

	private String id;
	private HashSet<IGNode> interferingNodes = new HashSet<IGNode>();
	
	private boolean active = true;
	private int register = -1;
	
	/**
	 * Constructs a IGNode object by setting its ID.
	 * 
	 * @param id the node ID
	 */
	public IGNode(String id) {
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		
		if(this == obj)	return true;
		if(obj == null)	return false;
		if(getClass() != obj.getClass()) return false;
		
		IGNode other = (IGNode) obj;
		
		if(id == null) {
			if(other.id != null) return false;
		} else if(!id.equals(other.id)) return false;

		return true;
	}
	
	/**
	 * @return the node ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the node ID to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the interfering nodes set
	 */
	public HashSet<IGNode> getInterferingNodes() {
		return interferingNodes;
	}

	/**
	 * @param interferingNodes the interfering nodes set to set
	 */
	public void setInterferingNodes(HashSet<IGNode> interferingNodes) {
		this.interferingNodes = interferingNodes;
	}

	/**
	 * @return whether this node is active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * @param active whether this node is active
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * @return the register number
	 */
	public int getRegister() {
		return register;
	}

	/**
	 * @param register the register number to set
	 */
	public void setRegister(int register) {
		this.register = register;
	}
}