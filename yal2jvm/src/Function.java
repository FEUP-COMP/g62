import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Function {

	private LinkedHashMap<String, Element> params;
	private LinkedHashMap<String, Element> vars;
	private Element returnElem;
	private String name;
	private int currentRegister;

	public Function(String name) {
		this.name = name;
		this.params = new LinkedHashMap<String, Element>();
		this.vars = new LinkedHashMap<String, Element>();
		this.returnElem = null;
		if(this.name.equals("main")) {
			this.currentRegister = 1;
		} else {
			this.currentRegister = 0;
		}
		
	}

	public String getName() {
		return name;
	}

	public void setReturnElem(Element returnElem) {
		this.returnElem = returnElem;
	}

	/**
	 * @return the function's return Element, can be null if no return exists
	 */
	public Element getReturnElem() {
		return this.returnElem;
	}

	public void addParam(String id, Element elem) {
		this.params.put(id, elem);
	}

	public void addVar(String id, Element elem) {
		this.vars.put(id, elem);
	}

	public LinkedHashMap<String, Element> getParams() {
		return params;
	}

	public LinkedHashMap<String, Element> getVars() {
		return vars;
	}

	public void assignRegisters() {

		Iterator<Entry<String, Element>> it = this.params.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Element> pair = (Map.Entry<String, Element>) it.next();
			Element e = pair.getValue();
			e.setRegister(this.currentRegister++);
		}
		
		//look for available register
		it = this.vars.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Element> pair = (Map.Entry<String, Element>) it.next();
			Element e = pair.getValue();
			if(e.getRegister() > this.currentRegister) {
				this.currentRegister = e.getRegister();
			}
		}

		it = this.vars.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Element> pair = (Map.Entry<String, Element>) it.next();
			Element e = pair.getValue();
			if(e.getRegister() == -1) {
				e.setRegister(this.currentRegister++);
			}
		}
	}
}
