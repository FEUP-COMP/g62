import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CallJVMGenerator extends JVMGenerator {
	
	private String returnType;

	public CallJVMGenerator(Function function, String moduleName, String returnType) {
		super(function, moduleName);
		this.returnType = returnType;
	}

	public void gen(String line) {
		//getting args
		String patternString = ".*?\\((.*)\\)";
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(line);
		matcher.find();
		String[] args = matcher.group(1).split("( )*;( )*");
		
		//check if function has no params
		patternString = ".*?\\[\\]";
		pattern = Pattern.compile(patternString);
		matcher = pattern.matcher(args[1]);
		String[] params;
		if(matcher.find()) {
			params = new String[0];
		} else {
			//getting params
			patternString = ".*?\\[(.*)\\]";
			pattern = Pattern.compile(patternString);
			matcher = pattern.matcher(args[1]);
			matcher.find();
			params = matcher.group(1).split("( )*,( )*");
		}
		
		for (int i = 0; i < params.length; i++) {
			loadValue(params[i]);
		}

		String[] module_function = args[0].split("\\.");
		JasminGenerator.write("invokestatic "+module_function[0]+"/"+module_function[1]+"(");
		
		String[] paramTypes = getParamTypes(params);
		for (int i = 0; i < paramTypes.length; i++) {
			JasminGenerator.write(paramTypes[i]);
		}
		
		JasminGenerator.write(")" + this.returnType + "\n");
	}
	
	public String[] getParamTypes(String[] params) {

		ArrayList<String> paramsList = new ArrayList<String>();
		
		for (int i = 0; i < params.length; i++) {
			String type = this.finder.getVarType(params[i]);
			if(type.equals("scalar") || type.equals("const")) {
				paramsList.add("I");
			} else if(type.equals("array")){
				paramsList.add("[I");
			} else if(type.equals("string")) {
				paramsList.add("Ljava/lang/String;");
			}
		}
		
		return paramsList.toArray(new String[paramsList.size()]);
	}

}
