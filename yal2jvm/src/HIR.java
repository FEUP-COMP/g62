import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Responsible for creating a high level intermediate representation and filling the symbol table.
 * A small amount of semantic analysis is done here since it's easier to check some conditions while
 * filling the symbol table.
 * Creation is based on the AST generated previously by parsing the input .yal file.
 */
public class HIR {

	private static final String lineBreak = "\r\n";
	private static final String fieldSeparator = ";";
	private static final String argsSeparator = ",";
	private static final String termSeparator = "#";
	
	// State variables
	enum NodeState {FUNCTION, FUNCTION_RET, FUNCTION_ARGS, ASSIGN_LHS, ASSIGN_RHS, CALL, WHILE, IF};
	private static final ArrayList<NodeState> assignStates = new ArrayList<NodeState>(Arrays.asList(NodeState.ASSIGN_LHS, NodeState.ASSIGN_RHS));
	private static final ArrayList<NodeState> exprStates = new ArrayList<NodeState>(Arrays.asList(NodeState.WHILE, NodeState.IF));
	private static final ArrayList<NodeState> functionStates = new ArrayList<NodeState>(Arrays.asList(NodeState.FUNCTION, NodeState.FUNCTION_RET, NodeState.FUNCTION_ARGS));
	private static final ArrayList<String> ignoreNodes = new ArrayList<String>(Arrays.asList("="));
	private static final ArrayList<String> arithOps = new ArrayList<String>(Arrays.asList("+", "-", "*", "/"));
	private static final ArrayList<String> binOps = new ArrayList<String>(Arrays.asList("&", "|", "^", ">>", "<<", ">>>"));
	private static final ArrayList<String> relaOps = new ArrayList<String>(Arrays.asList(">", "<", "<=", ">=", "==", "!="));

	// Path variables
	private String moduleName;

	// Parse variables
	private NodeState state;
	private NodeState previousState;
	private boolean isScalar;
	private boolean initialized = false;
	private boolean globalsArraySize = false;
	private String elementID;
	private String value;
	private String buildHIR = "";
	private String previousNode = "";
	private String arrIndex;
	private Element returnElem;
	private String currFunctionName;
	private String signPrefix = "";
	private String functionAssignType = "";
	private int termCount;
	private ArrayList<String> callArgs = new ArrayList<String>();
	
	// Use/def sets variables
	private int lineNumber;
	private HashMap<Integer, UseDefSet> varSets;
	private HashMap<String, HashMap<Integer, UseDefSet>> functionSets = new HashMap<String, HashMap<Integer, UseDefSet>>();
	
	// File output
	private FileWriter output;
	
	/**
	 * Creates a HIR object with the current module name, HIR folder path
	 * and HIR file extension. Useful for creating the HIR / symbol table
	 * or accessing the created HIR files.
	 */
	public HIR() {
		
		this.moduleName = YalParser.moduleName;

		// Create HIR files directory and current yal file directory
		YalUtility.getInstance().createDirIfNotExists(YalParser.hirFilesPath);
		YalUtility.getInstance().createDirIfNotExists(YalParser.hirFilesPath + "/" + YalParser.yalFilenameNoExt);
	}
	
	/**
	 * Generates HIR and symbol table based on the global declarations AST
	 * and the per function AST.
	 */
	public HashMap<String, HashMap<Integer, UseDefSet>> genIRAndSymbol() throws IOException {

		// Fetch AST for globals and functions
		ArrayList<SimpleNode> globals = YalParser.globalsAST;
		LinkedHashMap<String, SimpleNode> functions = YalParser.functionsAST;

		// Create HIR file for module globals
		this.output = new FileWriter(YalParser.buildFilePath(null, YalParser.hirFilesPath, YalParser.hirFileExt));

		for(SimpleNode node : globals) {
			recurseGlobals(node);
		}
		
		this.output.close();
		
		YalDebug.getInstance().printSymbolTable("Global variables:", YalParser.globalsTable);

		// Create one HIR file per function
		for(Map.Entry<String, SimpleNode> entry : functions.entrySet()) {
			
			this.output = new FileWriter(YalParser.buildFilePath(entry.getKey(), YalParser.hirFilesPath, YalParser.hirFileExt));
			
			// Reset function variables
			this.state = NodeState.FUNCTION;
			this.lineNumber = 1;
			this.varSets = new HashMap<Integer, UseDefSet>();
			
			this.recurseFunctions(entry.getValue());
			
			this.functionSets.put(this.currFunctionName, this.varSets);
			this.output.close();
			
			YalDebug.getInstance().printFunctionInfo(YalParser.functionTables.get(this.currFunctionName));
		}
		
		// Remove all non scalars from the use/def set
		this.cleanUseDefSet();
		
		return this.functionSets;
	}
	
	/**
	 * Recurses through a module's declaration nodes to build the HIR.
	 * 
	 * @param node the current node being visited
	 */
	private void recurseGlobals(SimpleNode node) throws IOException {

		String nodeName = yal2jvmTreeConstants.jjtNodeName[node.getId()].toLowerCase();
		
		this.parseDeclaration(node);
		
		for(int i = 0; i < node.jjtGetNumChildren(); i++) {
			recurseGlobals((SimpleNode) node.jjtGetChild(i));
		}
		
		// Finished recursive call for this declaration node, process
		if(nodeName.equals("declaration")) {
			this.declareHIR(node);
			this.resetParseVars();
		}
	}
	
	/**
	 * Recurses through a function's nodes to build the HIR.
	 * 
	 * @param node the current node being visited
	 */
	private void recurseFunctions(SimpleNode node) throws IOException {
		
		String nodeName = yal2jvmTreeConstants.jjtNodeName[node.getId()].toLowerCase();
		
		this.parseNode(node);
		
		for(int i = 0; i < node.jjtGetNumChildren(); i++) {
			recurseFunctions((SimpleNode) node.jjtGetChild(i));
		}

		// While node finished
		if(nodeName.equals("while")) {
			this.output.write("endwhile");
			this.output.write(HIR.lineBreak);
			this.lineNumber++;
			
		// If node finished
		} else if(nodeName.equals("if")) {
				this.output.write("endif");
				this.output.write(HIR.lineBreak);
				this.lineNumber++;
				
		// Call node finished
		} else if(nodeName.equals("call")) {

			// Reset state to previous state
			if(this.previousState != null) {
				this.state = this.previousState;
				this.previousState = null;
			}

			// Find id/args separator
			int separatorI = this.callArgs.indexOf(HIR.fieldSeparator);
			int iterateTo = (separatorI < 0) ? this.callArgs.size() : separatorI;

			// Build function id
			ArrayList<String> functionID = new ArrayList<String>();
			for(int i = 0; i < iterateTo; i++) {
				functionID.add(this.callArgs.get(i));
			}
			
			// Build argument list
			ArrayList<String> funcArgs = new ArrayList<String>();
			if(separatorI >= 0) {
				for(int i = separatorI + 1; i < this.callArgs.size(); i++) {
					funcArgs.add(this.callArgs.get(i));
					
					String arg = this.callArgs.get(i);
					if(!arg.contains("\"")) this.insertUseDefSet(this.callArgs.get(i), true); // Add function arguments as use
				}
			}

			// Build the function call string
			this.value = "";
			if(this.state.equals(NodeState.CALL)) {
				this.buildHIR += "call(";
			}
			
			if(functionID.size() == 1) {
				this.value += this.moduleName + ".";
				this.buildHIR += this.moduleName + ".";
			} else {
				this.value += functionID.get(0) + ".";
				this.buildHIR += functionID.get(0) + ".";
			}
			
			// Check for Call or nested Call
			if(this.state.equals(NodeState.CALL)) {
				this.value += functionID.get(functionID.size() - 1) + HIR.fieldSeparator + "[";
				this.buildHIR += functionID.get(functionID.size() - 1) + HIR.fieldSeparator + "[";
			} else {
				this.value += functionID.get(functionID.size() - 1) + "(";
				this.buildHIR += functionID.get(functionID.size() - 1) + "(";
			}

			// Build the function args string
			for(int i = 0; i < funcArgs.size(); i++) {
				
				if(i == funcArgs.size() - 1) {
					this.value += funcArgs.get(i);
					this.buildHIR += funcArgs.get(i);
				} else {
					this.value += funcArgs.get(i) + HIR.argsSeparator;
					this.buildHIR += funcArgs.get(i) + HIR.argsSeparator;
				}
				
			}
			
			// Check for Call or nested Call
			if(this.state.equals(NodeState.CALL)) {
				
				this.value += "])";
				this.buildHIR += "])";

				this.output.write(this.buildHIR);
				this.output.write(lineBreak);
				this.lineNumber++;
				this.resetParseVars();
				
			} else {

				// Check return type to write HIR for declarations
				if(YalParser.functionTables.containsKey(functionID.get(functionID.size() - 1))) {
					Element returnElem = YalParser.functionTables.get(functionID.get(functionID.size() - 1)).getReturnElem();
					if(returnElem != null) {
						if(returnElem.getType().equals("array")) {
							
							// Assigning array to function return
							if(this.functionAssignType.equals("")) {
								this.isScalar = false;
								this.functionAssignType = "array";
							} else this.functionAssignType = "";
						}
					}
				// External function call, use assign variable type as return type
				} else {
					
					VariableFinder varFinder = new VariableFinder(YalParser.functionTables.get(this.currFunctionName));
					if(varFinder.lookupVarType(this.elementID).equals(Element.ElemType.ARRAY)) {
						
						// Assigning array to function return
						if(this.functionAssignType.equals("")) {
							this.functionAssignType = "array";
						} else this.functionAssignType = "";
					}
				}

				this.value += ")";
				this.buildHIR += ")";
				this.previousNode = "";
				this.callArgs = new ArrayList<String>();
			}

		// Assign node finished
		} else if(nodeName.equals("assign")) {
			this.assignHIR();
			this.insertLocalTable(this.currFunctionName);
			this.resetParseVars();
			
		// Function node finished
		} else if(nodeName.equals("function")) {
			
			Function func = YalParser.functionTables.get(this.currFunctionName);
			Element elem = func.getReturnElem();

			if(elem != null) {
				
				// Check if function return was declared and is the correct type
				if(!this.checkIfType(elem.parseAsElemType(), this.currFunctionName, elem.getName())) {
					
					// Return not declared
					if(this.checkIfType(Element.ElemType.VOID, this.currFunctionName, elem.getName())) {
						String errMsg = "return variable for function \"" + this.currFunctionName + "\" not declared";
						this.addToErrors(node, errMsg);
					// Return type mismatch
					} else {
						String errMsg = "return variable for function \"" + this.currFunctionName + "\" does not match the return type \"" + elem.parseAsElemType() + "\"";
						this.addToErrors(node, errMsg);
					}
				}
			}

			this.resetParseVars();
		}
	}
	
	/**
	 * Parses a function node to build the HIR.
	 * 
	 * @param node the current node being visited
	 */
	private void parseNode(SimpleNode node) throws IOException {
		
		String nodeName = yal2jvmTreeConstants.jjtNodeName[node.getId()].toLowerCase();
		String nodeValue = "";
		
		// Assign AST value if parsing a leaf
		if(node.jjtGetNumChildren() == 0 && !nodeName.equals("stmtlst")) {
			nodeValue = ((String) node.jjtGetValue()).toLowerCase();
		}

		switch(nodeName) {

		case "scalarelem":
			this.isScalar = true;
			if(this.state.equals(NodeState.FUNCTION)) this.state = NodeState.FUNCTION_RET;
			break;
			
		case "arrayelem":
			this.isScalar = false;
			if(this.state.equals(NodeState.FUNCTION)) this.state = NodeState.FUNCTION_RET;
			break;
		
		case "assign":
			this.state = NodeState.ASSIGN_LHS;
			break;
		
		case "scalaraccess":
			if(this.state.equals(NodeState.ASSIGN_LHS)) this.isScalar = true;
			break;
		
		case "arrayaccess":
			if(this.state.equals(NodeState.ASSIGN_LHS)) {
				this.isScalar = false;
				this.buildHIR = "astore(";
			}
			break;
			
		case "varlist":
			this.state = NodeState.FUNCTION_ARGS;
			break;
			
		case "id":
		case "string":

			// Function return element found
			if(this.state.equals(NodeState.FUNCTION_RET)) {
				
				String elemType;
				if(this.isScalar) {
				  elemType = "scalar";
				} else {
					elemType = "array";
				}
				
				this.returnElem = new Element(nodeValue, elemType, false);
				this.state = NodeState.FUNCTION;
				break;
			// Function name found
			} else if(this.state.equals(NodeState.FUNCTION)) {
				
				this.currFunctionName = nodeValue;
				this.insertFunctionTable(this.currFunctionName);
				break;
			// Function argument list
			} else if(this.state.equals(NodeState.FUNCTION_ARGS)) {
				
				String elemType;
				if(this.isScalar) {
				  elemType = "scalar";
				} else {
					elemType = "array";
				}
				
				Element elem = new Element(nodeValue, elemType, false);
				this.insertParamTable(node, elem, this.currFunctionName);
				break;
			}
			
			// Accessing array using scalar variable
			if(this.previousEquals("index")) {
				
				if(this.state.equals(NodeState.ASSIGN_LHS)) {
					this.buildHIR += HIR.fieldSeparator + nodeValue + HIR.fieldSeparator;
					this.arrIndex = nodeValue;
				} else {
					this.value += "[" + nodeValue + "]";
					this.buildHIR += "[" + nodeValue + "]";
				}
				
				this.insertUseDefSet(nodeValue, true); // Insert index as use
				break;
			} else this.previousEquals("term");
			
			// Function call parameters
			if(this.state.equals(NodeState.CALL)) {
				this.buildHIR += this.signPrefix;
				this.signPrefix = "";
				this.callArgs.add(nodeValue);
			// LHS assignment ID
			} else if(this.state.equals(NodeState.ASSIGN_LHS)) {
						
				if(this.checkIfType(Element.ElemType.ARRAY, this.currFunctionName, nodeValue) &&
						!this.buildHIR.contains("astore")) {
					this.buildHIR = "afill(";
				}
				
				this.elementID = nodeValue;
				this.insertUseDefSet(nodeValue, false); // Insert assign as def
			}
			
			// Standard behaviour except for Call nodes
			if(!this.state.equals(NodeState.CALL)) {
				this.value = this.signPrefix + nodeValue;
				this.buildHIR += this.signPrefix + nodeValue;
				this.signPrefix = "";
				
				if(!this.state.equals(NodeState.ASSIGN_LHS)) this.insertUseDefSet(nodeValue, true);
			}
			break;
			
		case "size":
			this.value += ".size";
			this.buildHIR += ".size";
			break;
			
		case "argumentlist":
			this.callArgs.add(HIR.fieldSeparator);
			break;
			
		case "stmtlst":
			if(HIR.functionStates.contains(this.state)) {
				this.state = NodeState.FUNCTION;
			} else if(HIR.exprStates.contains(this.state)) {
				this.output.write(this.buildHIR + ")");
				this.output.write(HIR.lineBreak);
				this.lineNumber++;
				this.resetParseVars();
			}
			
		case "arraysize":
			if(this.buildHIR.contains("afill")) this.buildHIR = "";
			this.isScalar = false;
			break;
			
		case "index":
			if(this.buildHIR.contains("afill") && this.state.equals(NodeState.ASSIGN_LHS)) this.buildHIR = "";
			this.previousNode = "index";
			break;
			
		case "rhs":
			if(HIR.assignStates.contains(this.state)) this.state = NodeState.ASSIGN_RHS;
			break;
			
		case "call":
			if(!this.state.equals(NodeState.FUNCTION)) this.previousState = this.state;
			this.state = NodeState.CALL;
			break;
			
		case "while":
			this.state = NodeState.WHILE;
			this.buildHIR = "while(";
			break;
			
		case "if":
			this.state = NodeState.IF;
			this.buildHIR = "if(";
			break;
		
		case "elsebody":
			this.output.write("else");
			this.output.write(HIR.lineBreak);
			this.lineNumber++;
			break;

		case "term":
			this.previousNode = "term";
			this.termCount++;
			break;
			
		default:
			
			if(node.jjtGetNumChildren() != 0) break;
			
			// Variable already declared as array, might be a fill operation
			if(this.buildHIR.contains("afill")) {
				if (this.checkIfType(Element.ElemType.ARRAY, this.currFunctionName, this.elementID)) {

					if(HIR.ignoreNodes.contains(nodeValue)) this.buildHIR += HIR.fieldSeparator;
					else if(this.previousEquals("index")) {
						this.buildHIR += "[" + nodeValue + "]";
					} else this.buildHIR += nodeValue;
					
					if(this.state.equals(NodeState.CALL)) this.callArgs.add(nodeValue);
					break;
				}
			}
			
			// Parse leaf if it contains a numeric value
			if(YalUtility.getInstance().isInteger(nodeValue)) {
				
				// Assignment to array position
				if(this.previousEquals("index")) {
					if(this.state.equals(NodeState.ASSIGN_LHS)) {
						this.arrIndex = nodeValue;
						this.buildHIR += HIR.fieldSeparator + nodeValue + HIR.fieldSeparator;
					} else if(this.state.equals(NodeState.ASSIGN_RHS)) {
						this.value += "[" + nodeValue + "]";
						this.buildHIR += "[" + nodeValue + "]";
					} else if(HIR.exprStates.contains(this.state)) {
						this.buildHIR += "[" + nodeValue + "]";
					}
				} else if(this.state.equals(NodeState.CALL)) {
					this.callArgs.add(nodeValue);
				} else {
					
					this.initialized = true;
				
					this.value = this.signPrefix + nodeValue;
					this.signPrefix = "";
					this.buildHIR += this.value;
				}
			} else if(!HIR.ignoreNodes.contains(nodeValue)) {
				
				// If arithmetic operator found, change HIR to arith, value contains the first operand and elemenID the LHS
				if(HIR.arithOps.contains(nodeValue)) {
					
					if(this.previousEquals("term")) {
						this.signPrefix = nodeValue;
					} else if(HIR.exprStates.contains(this.state)) {
						this.buildHIR += HIR.termSeparator + nodeValue + HIR.termSeparator;
					} else {
						this.buildHIR = "arith(" + this.elementID;
						if(!this.isScalar) this.buildHIR += "[" + this.arrIndex + "]";
						this.buildHIR += HIR.fieldSeparator + this.value + HIR.termSeparator + nodeValue + HIR.termSeparator;
					}
				// If binary operator found, change HIR to bin, value contains the first operand and elemenID the LHS
				} else if(HIR.binOps.contains(nodeValue)) {

					if(HIR.exprStates.contains(this.state)) {
						this.value += nodeValue;
						this.buildHIR += HIR.termSeparator + nodeValue + HIR.termSeparator;
					} else {
						this.buildHIR = "bin(" + this.elementID;
						if(!this.isScalar) this.buildHIR += "[" + this.arrIndex + "]";
						this.buildHIR += HIR.fieldSeparator + this.value + HIR.termSeparator + nodeValue + HIR.termSeparator;
					}
				} else if(HIR.relaOps.contains(nodeValue)) {
					this.buildHIR += nodeValue;
				} else this.buildHIR += this.value;
			}
			
			this.previousEquals("term");
			break;
		}
	}
	
	/**
	 * Parses the module's declaration nodes in search of
	 * global variables declarations.
	 * 
	 * @param node the current node being visited
	 */
	private void parseDeclaration(SimpleNode node) {
	
		String nodeName = yal2jvmTreeConstants.jjtNodeName[node.getId()].toLowerCase();
		String nodeValue = "";
		
		// Assign AST value if parsing a leaf
		if(node.jjtGetNumChildren() == 0) {
			nodeValue = ((String) node.jjtGetValue()).toLowerCase();
		}
		
		switch(nodeName) {
		
		case "scalarelem":
			this.isScalar = true;
			break;
		
		case "arrayelem":
			this.isScalar = false;
			break;
			
		case "id":
			this.elementID = nodeValue;
			break;
			
		case "arraysize":
			this.isScalar = false;
			this.globalsArraySize = true;
			break;
		
		default:
			
			if(node.jjtGetNumChildren() != 0) break;
			
			// Get leaf value if it's numeric
			if(YalUtility.getInstance().isInteger((String) node.jjtGetValue())) {
				this.initialized = true;
				this.value = nodeValue;
			}
			break;
		}
	}
	
	/**
	 * Handles writing adeclare/ideclare/afill HIR instructions for globals.
	 * 
	 * @param node the current node being visited
	 */
	private void declareHIR(SimpleNode node) throws IOException {
		
		Element elem = YalParser.globalsTable.get(this.elementID);
		
		// If element is already in globals symbol table, check if array fill
		if(elem != null && this.initialized && !this.globalsArraySize) {
			elem.setInitialize(this.initialized);
			if(elem.getType().equals("array")) {
				this.output.write("afill(" + elem.getName() + HIR.fieldSeparator + this.value + ")" + lineBreak);
				this.insertGlobalTable(node, true);
				return;
			}
		}
		
		if(this.isScalar) this.output.write("ideclare(" + this.elementID);
		else this.output.write("adeclare(" + this.elementID);
		
		if(this.initialized) {
			this.output.write(HIR.fieldSeparator + this.value + ")");
		} else this.output.write(")");
		
		this.output.write(lineBreak);
		this.insertGlobalTable(node, false);
	}

	/**
	 * Handles writing adeclare/ideclare HIR instructions. Separates arguments
	 * with a term separator for easier parsing in code generation.
	 */
	private void assignHIR() throws IOException {

		// Afill args separator
		if(this.buildHIR.contains("afill") && this.termCount == 2) {

			int offset = this.buildHIR.indexOf(">>>");
			if(offset >= 0) {
				String suffix = this.buildHIR.substring(offset + 3);
				this.buildHIR = this.buildHIR.substring(0, offset) + HIR.termSeparator + ">>>" + HIR.termSeparator + suffix;
			} else {
				offset = this.buildHIR.indexOf("/");
				if(offset >= 0) {
					String suffix = this.buildHIR.substring(offset + 1);
					this.buildHIR = this.buildHIR.substring(0, offset) + HIR.termSeparator + "/" + HIR.termSeparator + suffix;
				} else {
					offset = this.buildHIR.indexOf("*");
					if(offset >= 0) {
						String suffix = this.buildHIR.substring(offset + 1);
						this.buildHIR = this.buildHIR.substring(0, offset) + HIR.termSeparator + "*" + HIR.termSeparator + suffix;
					} else {
						offset = this.buildHIR.indexOf("&");
						if(offset >= 0) {
							String suffix = this.buildHIR.substring(offset + 1);
							this.buildHIR = this.buildHIR.substring(0, offset) + HIR.termSeparator + "&" + HIR.termSeparator + suffix;
						} else {
							offset = this.buildHIR.indexOf("|");
							if(offset >= 0) {
								String suffix = this.buildHIR.substring(offset + 1);
								this.buildHIR = this.buildHIR.substring(0, offset) + HIR.termSeparator + "|" + HIR.termSeparator + suffix;
							} else {
								offset = this.buildHIR.indexOf(">>");
								if(offset >= 0) {
									String suffix = this.buildHIR.substring(offset + 2);
									this.buildHIR = this.buildHIR.substring(0, offset) + HIR.termSeparator + ">>" + HIR.termSeparator + suffix;
								} else {
									offset = this.buildHIR.indexOf("<<");
									if(offset >= 0) {
										String suffix = this.buildHIR.substring(offset + 2);
										this.buildHIR = this.buildHIR.substring(0, offset) + HIR.termSeparator + "<<" + HIR.termSeparator + suffix;
									} else {
										offset = this.buildHIR.indexOf("^");
										if(offset >= 0) {
											String suffix = this.buildHIR.substring(offset + 1);
											this.buildHIR = this.buildHIR.substring(0, offset) + HIR.termSeparator + "^" + HIR.termSeparator + suffix;
										}
									}
								} 
							}
						}
					}
				}
			}
			
			// +/- operators
			if(offset < 0) {
				
				int fieldOffset = this.buildHIR.indexOf(HIR.fieldSeparator);
				String operator = "";
				offset = this.buildHIR.indexOf("+");
				if(offset < 0) {
					offset = this.buildHIR.indexOf("-");
					operator = "-";
				} else operator = "+";
				
				// +/- right after =
				String prefix = "";
				String remainder = "";
				if(fieldOffset + 1 == offset) {
					prefix = this.buildHIR.substring(0, offset + 1);
					remainder = this.buildHIR.substring(offset + 1);

				}
				
				if(!remainder.equals("")) {
					offset = remainder.indexOf("+");
					if(offset < 0) {
						offset = remainder.indexOf("-");
						operator = "-";
					} else operator = "+";
					
					offset += prefix.length();
				}
				
				String suffix = this.buildHIR.substring(offset + 1);
				this.buildHIR = this.buildHIR.substring(0, offset) + HIR.termSeparator + operator + HIR.termSeparator + suffix;
			}
		}
		
		if(!this.buildHIR.contains("astore") && !this.buildHIR.contains("arith") && !this.buildHIR.contains("bin")) {

			// Assigning array to function return
			if(this.functionAssignType.equals("array") && this.termCount == 1) {
				this.buildHIR = "adeclare(" + this.elementID + HIR.fieldSeparator + this.value;
			// Regular assign
			} else if(!this.buildHIR.contains("afill")) {
				if (this.isScalar) {
					this.buildHIR = "ideclare(" + this.elementID + HIR.fieldSeparator + this.value;
				} else this.buildHIR = "adeclare(" + this.elementID + HIR.fieldSeparator + this.value;
			}
		}

		this.buildHIR += ")";
		this.functionAssignType = "";
		this.output.write(this.buildHIR);
		this.output.write(lineBreak);
		this.lineNumber++;
	}
	
	/**
	 * Inserts a variable into the globals symbol table. Verifies
	 * duplicate global error if operation isn't an array fill.
	 * 
	 * @param node the current node being visited
	 * @param isArrayFill whether an array fill is being performed
	 */
	private void insertGlobalTable(SimpleNode node, boolean isArrayFill) {
		
		if(YalParser.globalsTable.containsKey(this.elementID)) {
			if(!isArrayFill) {
				String msg = "duplicate global declared - \"" + this.elementID + "\"";
				this.addToErrors(node, msg);
			}
			return;
		}
		
		LinkedHashMap<String, Element> globalTables = YalParser.globalsTable;
		
		String type = this.isScalar ? "scalar" : "array";
		globalTables.put(this.elementID, new Element(this.elementID, type, this.initialized));
	}
	
	/**
	 * Inserts a new function object into the function table.
	 * 
	 * @param functionName the function name
	 */
	private void insertFunctionTable(String functionName) {
		
		if(YalParser.functionTables.containsKey(this.currFunctionName)) return;
		
		Function func = new Function(functionName);
		func.setReturnElem(this.returnElem);
		YalParser.functionTables.put(functionName, func);
	}
	
	/**
	 * Inserts a variable into the function parameters symbol table.
	 * Verifies duplicate param error.
	 * 
	 * @param node the current node being visited
	 * @param elem the element to insert
	 * @param functionName the function name
	 */
	private void insertParamTable(SimpleNode node, Element elem, String functionName) {
		
		Function func = YalParser.functionTables.get(functionName);
		
		if(func.getParams().containsKey(elem.getName())) {
			String msg = "duplicate function parameter declared - \"" + elem.getName() + "\"";
			this.addToErrors(node, msg);
			return;
		}
		func.getParams().put(elem.getName(), elem);
	}
	
	/**
	 * Inserts a variable into the function locals symbol table
	 * if it doesn't already exist.
	 * 
	 * @param functionName the function name
	 */
	private void insertLocalTable(String functionName) {

		if(!this.buildHIR.contains("arith") && !this.buildHIR.contains("bin")
				&& !this.buildHIR.contains("ideclare") && !this.buildHIR.contains("adeclare")) return;
		
		// Check if indexed array access
		if(this.buildHIR.contains("arith") || this.buildHIR.contains("bin")) {
			if(this.buildHIR.split(";")[0].matches(".*\\[.*\\]")) return;
		}
		
		Function func = YalParser.functionTables.get(functionName);
		VariableFinder varFinder = new VariableFinder(func);

		if(!varFinder.lookupVarType(this.elementID).equals(Element.ElemType.VOID)) return;

		// TODO refactor for enum element
		String elemType;
		if(this.isScalar) {
			elemType = "scalar";
		} else {
			elemType = "array";
		}

		func.getVars().put(this.elementID, new Element(this.elementID, elemType, true));
	}
	
	/**
	 * Checks if the given function contains the specified variable with
	 * the given type.
	 * 
	 * @param desiredType the element type to check against
	 * @param functionName the function name
	 * @param varName the variable name
	 * @return whether the type was as expected
	 */
	private boolean checkIfType(Element.ElemType desiredType, String functionName, String varName) {

		if(!YalParser.functionTables.containsKey(functionName)) return false;
		VariableFinder varFinder = new VariableFinder(YalParser.functionTables.get(functionName));
		return varFinder.lookupVarType(varName) == desiredType;
	}

	/**
	 * Removes all non scalar variables from the use/def sets and
	 * also removes any non local variables.
	 */
	private void cleanUseDefSet() {
		
		// For each function
		for(Map.Entry<String, HashMap<Integer, UseDefSet>> sets : this.functionSets.entrySet()) {
			String funcName = sets.getKey();
			
			// For each function line
			for(Map.Entry<Integer, UseDefSet> funcSet : sets.getValue().entrySet()) {

				// Remove elements if they aren't local
				funcSet.getValue().getUseSet().removeIf(varName -> !this.checkIfLocal(funcName, varName));
				funcSet.getValue().getDefSet().removeIf(varName -> !this.checkIfLocal(funcName, varName));
				
				// Remove elements if they aren't scalar
				funcSet.getValue().getUseSet().removeIf(varName -> !this.checkIfType(Element.ElemType.SCALAR, funcName, varName));
				funcSet.getValue().getDefSet().removeIf(varName -> !this.checkIfType(Element.ElemType.SCALAR, funcName, varName));
			}
		}
	}
	
	/**
	 * Checks if a given variable is function local.
	 * 
	 * @param functionName the function name
	 * @param varName the variable name to check
	 * @return whether the variable is function local
	 */
	private boolean checkIfLocal(String functionName, String varName) {
		
		VariableFinder varFinder = new VariableFinder(YalParser.functionTables.get(functionName));
		return varFinder.isLocal(varName);
	}
	
	/**
	 * Inserts a variable into the use/def set for the current line.
	 * 
	 * @param varName the variable name to insert
	 * @param isUse whether the variable is being used or defined
	 */
	private void insertUseDefSet(String varName, boolean isUse) {
		
		// Create set if it doesn't exist
		if(!this.varSets.containsKey(this.lineNumber)) {
			this.varSets.put(this.lineNumber, new UseDefSet());
		}
		
		if(isUse) this.varSets.get(this.lineNumber).getUseSet().add(varName);
		else this.varSets.get(this.lineNumber).getDefSet().add(varName);
	}
	
	/**
	 * Check whether the previous node string is equal to the specified string.
	 * Resets the previous node string if true.
	 * 
	 * @param toCheck the string to check
	 * @return whether the previous node equals the specified string
	 */
	private boolean previousEquals(String toCheck) {
		boolean b = this.previousNode.equals(toCheck);
		if(b) this.previousNode = "";
		return b;
	}
	
	/**
	 * Resets parser variables that affect parsing decisions.
	 */
	private void resetParseVars() {

		this.initialized = false;
		this.globalsArraySize = false;
		this.buildHIR = "";
		this.previousNode = "";
		this.state = NodeState.FUNCTION;
		this.elementID = "";
		this.signPrefix = "";
		this.termCount = 0;
		this.returnElem = null;
		this.callArgs = new ArrayList<String>();
	}
	
	/**
	 * Adds an error to the error list for later printing.
	 * 
	 * @param node the current node being visited
	 * @param message the error message
	 */
	private void addToErrors(SimpleNode node, String message) {

		String errMsg = "Line " + node.getLineNumber() + ": " + message;
		YalParser.errorMessages.put(YalParser.semanticErrCount, errMsg);
		YalParser.semanticErrCount++;
	}
	
	/******************************************************************************************
	 * 
	 * GETTERS / SETTERS
	 * 
	 * 
	 */
	
	/**
	 * @return the current parser state
	 */
	public NodeState getState() {
		return state;
	}

	/**
	 * @return whether the current variable being parsed is a scalar or not
	 */
	public boolean isScalar() {
		return isScalar;
	}

	/**
	 * @return whether the current variable being parsed is initialized or not
	 */
	public boolean isInitialized() {
		return initialized;
	}

	/**
	 * @return the current element ID being parsed
	 */
	public String getElementID() {
		return elementID;
	}

	/**
	 * @return the current element value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @return the string representation of the HIR currently being built
	 */
	public String getBuildHIR() {
		return buildHIR;
	}

	/**
	 * @return the previous node name parsed
	 */
	public String getPreviousNode() {
		return previousNode;
	}

	/**
	 * @return the latest array index found by the AST parser
	 */
	public String getArrIndex() {
		return arrIndex;
	}
}
