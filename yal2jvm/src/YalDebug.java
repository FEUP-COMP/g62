import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class YalDebug {
	
	// Debug flags
	private static final boolean symbolTablePrint = true;
	private static final boolean syntaxDebugExit = false;
	private static final boolean registerAllocationPrint = true;
	private boolean debugFlag = false;
	
	private static YalDebug singleton = new YalDebug();
	
	/**
	 * Private constructor for singleton pattern.
	 */
	private YalDebug() {}
	
	/**
	 * @return the singleton instance of the class
	 */
	public static YalDebug getInstance() {
		return singleton;
	}
	
	/**
	 * Prints a symbol table.
	 * 
	 * @param header the table description header
	 * @param table the symbol table to print
	 */
	public void printSymbolTable(String header, HashMap<String, Element> table) {
		
		if(!this.debugFlag || !YalDebug.symbolTablePrint) return;
		
		System.out.println(header);
		for(Map.Entry<String, Element> entry : table.entrySet()) {
			System.out.println("\t" + entry.getValue().toString());
		}
		
		System.out.println();
	}
	
	/**
	 * Prints debug info about a Function object.
	 * 
	 * @param function the function object to print debug info of
	 */
	public void printFunctionInfo(Function function) {
		
		if(!this.debugFlag || !YalDebug.symbolTablePrint) return;
		
		System.out.print("Function: " + function.getName());
		if(function.getReturnElem() != null) System.out.print(" returns " + function.getReturnElem().toString());
		System.out.println();
		if(function.getParams().size() > 0) this.printSymbolTable("Function params:", function.getParams());
		if(function.getVars().size() > 0) this.printSymbolTable("Function locals:", function.getVars());
	}
	
	/**
	 * Prints an AST node info.
	 * 
	 * @param node the AST node to print info of
	 */
	public void printNodeInfo(SimpleNode node) {
		
		if(!this.debugFlag) return;
		
		System.out.println("Node name: " + node.toString());
		System.out.println("Node value: " + node.jjtGetValue());
	}
	
	/**
	 * Calls the dump method on the received node.
	 * 
	 * @param node the node to dump contents of
	 */
	public void dumpAST(SimpleNode node) {
		
		if(!this.debugFlag) return;
		
		node.dump("");
	}

	/**
	 * Exits with specific values so script files can run automated testing.
	 * Exit value 0 on success, -1 on all expected errors found and -2 on missed errors.
	 * 
	 * @param totalErrors the total number of errors expected
	 */
	public void syntaxDebugExit(int totalErrors)	{
		
		if(!this.debugFlag || !YalDebug.syntaxDebugExit) return;
		
		if(totalErrors < 0) totalErrors = 0;
		
		if (YalParser.errorCount > 0) {
			System.out.println("yal file parsed with " + YalParser.errorCount + " / " + totalErrors + " errors.");
			int exitValue = (YalParser.errorCount == totalErrors) ? - 1 : - 2;
			System.exit(exitValue);
		} else {
			System.out.println("yal file parsed successfully!");
			System.exit(0);
		}
	}

	/**
	 * Prints the variables being used to convert an AST to a HIR.
	 * 
	 * @param hir the HIR object handling the conversion
	 */
	public void printHIRParseVars(HIR hir) {
		
		if(!this.debugFlag) return;
		
		System.out.println("HIR parse values:");
		System.out.println("\t state: " + hir.getState().toString());
		System.out.println("\t scalar: " + hir.isScalar());
		System.out.println("\t init: " + hir.isInitialized());
		System.out.println("\t node id: " + hir.getElementID());
		System.out.println("\t node value: " + hir.getValue());
		System.out.println("\t latest index: " + hir.getArrIndex());
		System.out.println("\t previous node: " + hir.getPreviousNode());
	}
	
	/**
	 * Prints the current string being built by the HIR.
	 * 
	 * @param hir the HIR object handling the conversion
	 */
	public void printHIRStringBuild(HIR hir) {
		
		if(!this.debugFlag) return;

		System.out.println("HIR string: " + hir.getBuildHIR());
	}
	
	/**
	 * Prints all the sets of initialized local variables.
	 * 
	 * @param stack the stack of sets of initialized local variables
	 */
	public void printLocalsStack(ArrayList<HashSet<String>> stack) {
		
		if(!this.debugFlag) return;
		
		int i = 0;
		for(HashSet<String> set : stack) {
			String print = "Set " + i + " ";
			i++;
			for(String s : set) {
				print += s + " ";
			}
			System.out.println(print);
		}
		
		System.out.println();
	}

	/**
	 * Prints branch start/end lines.
	 * 
	 * @param branches the map containing all the branch info
	 */
	public void printBranchInfo(HashMap<String, ConditionalInfo> branches) {
		
		if(!this.debugFlag || !YalDebug.registerAllocationPrint) return;
		
		for(Map.Entry<String, ConditionalInfo> entry : branches.entrySet()) {
			
			String print = entry.getKey();
			ConditionalInfo condInfo = entry.getValue();
			
			// IF statement
			if(condInfo.getIfLine() != 0) {
				print += " if: " + condInfo.getIfLine();
				print += " else: " + condInfo.getElseLine();
				print += " endif: " + condInfo.getIfEndLine();
		    // WHILE statement
			} else {
				print += " while: " + condInfo.getWhileLine();
				print += " endwhile: " + condInfo.getWhileEndLine();
			}

			System.out.println(print);
		}
		
		System.out.println();
	}
	
	/**
	 * Prints the use/def sets of a function.
	 * 
	 * @param functionName the function name
	 * @param varSets the use/def sets for the function
	 */
	public void printUseDefSet(String functionName, HashMap<Integer, UseDefSet> varSets) {
		
		if(!this.debugFlag || !YalDebug.registerAllocationPrint) return;
		
		System.out.println("Function " + functionName + "()");
		
		for(Map.Entry<Integer, UseDefSet> entry : varSets.entrySet()) {
			
			System.out.println("Line " + entry.getKey());

			UseDefSet useDef = entry.getValue();
			String print;
			
			// Print use set
			if(useDef.getUseSet().size() > 0) {
			
				print = "\tuse: ";
				for(String s : useDef.getUseSet()) {
					print += s + " ";
				}
				System.out.println(print);
			}
			
			// Print def set
			if(useDef.getDefSet().size() > 0) {
				
				print = "\tdef: ";
				for(String s : useDef.getDefSet()) {
					print += s + " ";
				}
				System.out.println(print);
			}
		}
		
		System.out.println();
	}

	/**
	 * Prints the successors for each function line.
	 * 
	 * @param successorList the map of successors
	 */
	public void printSuccessorList(HashMap<Integer, HashSet<Integer>> successorList) {
		
		if(!this.debugFlag || !YalDebug.registerAllocationPrint) return;
		
		for(Map.Entry<Integer, HashSet<Integer>> entry : successorList.entrySet()) {
			
			String print = "Line " + entry.getKey() + ": ";
			for(Integer num : entry.getValue()) {
				print += num + " ";
			}
			System.out.println(print);
		}
		
		System.out.println();
	}
	
	/**
	 * Prints the in/out sets of a function.
	 * 
	 * @param iterationCount the iteration number
	 * @param inOutSets the in/out sets for the function
	 */
	public void printInOutSets(int iterationCount, HashMap<Integer, InOutSet> inOutSets) {
		
		if(!this.debugFlag || !YalDebug.registerAllocationPrint) return;
		
		System.out.println("Iteration " + iterationCount);
		
		for(Map.Entry<Integer, InOutSet> entry : inOutSets.entrySet()) {

			InOutSet inOut = entry.getValue();
			if(inOut.getOutSet().size() != 0 || inOut.getInSet().size() != 0) System.out.println("Line " + entry.getKey());
			
			String print;
			
			// Print out set
			if(inOut.getOutSet().size() > 0) {
			
				print = "\tout: ";
				for(String s : inOut.getOutSet()) {
					print += s + " ";
				}
				System.out.println(print);
			}
			
			// Print in set
			if(inOut.getInSet().size() > 0) {
				
				print = "\tin: ";
				for(String s : inOut.getInSet()) {
					print += s + " ";
				}
				System.out.println(print);
			}
		}
		
		System.out.println();
	}
	
	/**
	 * Prints an interference graph by printing its elements and
	 * the interfering nodes for each node.
	 * 
	 * @param graph the graph to print
	 * @param ignoreActive whether to ignore the node's active status
	 */
	public void printInterferenceGraph(HashMap<String, IGNode> graph, boolean ignoreActive) {
		
		if(!this.debugFlag || !YalDebug.registerAllocationPrint) return;
		
		String print = "Graph elements: ";
		for(Map.Entry<String, IGNode> entry : graph.entrySet()) {
			if(ignoreActive || entry.getValue().isActive()) print += entry.getValue().getId() + " ";
		}
		System.out.println(print);
		print = "";
		
		// Print interfering nodes for each node
		for(Map.Entry<String, IGNode> entry : graph.entrySet()) {
			
			if(ignoreActive || entry.getValue().isActive()) print = "Interference for " + entry.getValue().getId() + ": ";
			for(IGNode node : entry.getValue().getInterferingNodes()) {
				if(ignoreActive || node.isActive()) print += node.getId() + " ";
			}
			System.out.println(print);
			print = "";
		}

		System.out.println();
	}
	
	/**
	 * Prints the register allocation for a specific function.
	 * 
	 * @param functionName the function name
	 * @param graph the graph to print
	 */
	public void printRegisterAllocation(String functionName, HashMap<String, IGNode> graph) {
		
		if(!this.debugFlag || !YalDebug.registerAllocationPrint) return;
		
		System.out.println("Function " + functionName + "() register allocation");
		for(Map.Entry<String, IGNode> entry : graph.entrySet()) {	
			System.out.println("\t" + entry.getValue().getId() + ": " + entry.getValue().getRegister());
		}
		
		System.out.println();
	}
	
	/**
	 * Prints the IDs of the nodes on the supplied stack.
	 * 
	 * @param nodeStack the stack of nodes to print
	 */
	public void printNodeStack(Deque<IGNode> nodeStack) {
		
		if(!this.debugFlag || !YalDebug.registerAllocationPrint) return;
		
		String print = "Node stack: ";
		for(IGNode node : nodeStack) {
			print += node.getId() + " ";
		}
		
		System.out.println(print);
		System.out.println();
	}
	
	/**
	 * Prints a set of strings.
	 * 
	 * @param set the set to print
	 */
	public void printSet(HashSet<String> set) {
		
		if(!this.debugFlag) return;
		
		String print = "";
		for(String s : set) {
			print += s + " ";
		}
		System.out.println(print);
	}
	
	/**
	 * Prints quick user friendly status of the compiler flags.
	 * 
	 * @param localVarFlag whether register allocation algorithms are being used
	 * @param optimizeFlag whether optimizations are performed
	 * @param localVarCount the total number of registers available
	 */
	public void printFlagStatus(boolean localVarFlag, boolean optimizeFlag, int localVarCount) {
		
		if(!this.debugFlag) return;
		
		String print = "Flags:";
		if(localVarFlag) print += " local(" + localVarCount + ")";
		if(optimizeFlag) {
			if(localVarFlag) print += " |";
			print += " optimize";
		}
		
		if(this.debugFlag) {
			if(optimizeFlag || localVarFlag) print += " |";
			print += " debug";
		}
		
		System.out.println(print);
		System.out.println();
	}

	/**
	 * @param debugFlag whether to print debugging info or not
	 */
	public void setDebugFlag(boolean debugFlag) {
		this.debugFlag = debugFlag;
	}
}
