import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IfJVMGenerator extends JVMGenerator {
	
	private static String[] operators = {">","<","<=",">=","==","!="};
	private static String[] operatorsInstructions = {"if_icmple","if_icmpge","if_icmpgt","if_icmplt",
			"if_icmpne","if_icmpeq"};
	
	private int loopNumber;

	public IfJVMGenerator(Function function, String moduleName, int loopNumber) {
		super(function, moduleName);
		this.loopNumber = loopNumber;
	}
	
	@Override
	public void gen(String line) {
		String patternString = ".*?\\((.*)\\)";
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(line);
		matcher.find();
		
		String args = matcher.group(1);
		
		patternString = "";
		for (int i = 0; i < operators.length; i++) {
			patternString += "((?<=(" + operators[i] +  "(?!=)))|(?=(" + operators[i] + "(?!=))))";
			
			if(i != operators.length - 1) {
				patternString += "|";
			}
		}
		
		String[] ops = args.split(patternString);
		
		for(int i = 0; i < ops.length; i++) {
			ops[i] = ops[i].replaceAll("\\s+","");
		}
		
		loadValue(ops[0],"I");
		loadValue(ops[2],"I");
		writeOperation(ops[1]);
		
		JasminGenerator.write("\n");
	}
	
	private void writeOperation(String operator) {
		int index = Arrays.asList(operators).indexOf(operator);
		
		if(index == -1) {
			return;
		}
		
		String instruction = operatorsInstructions[index];
		
		JasminGenerator.write(instruction + " loop" + loopNumber + "_end\n");
	}

}
