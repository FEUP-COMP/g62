ECHO OFF
@RD /S /Q "%~dp0\..\compiledYals"
for /f %%f in ('dir /b "%~dp0\..\jasminFiles"') do ^
for /f %%g in ('dir /b "%~dp0\..\jasminFiles\%%f"') do ^
java -jar ..\jasmin.jar -d ..\compiledYals ..\jasminFiles\%%f\%%g
copy /Y "%~dp0\..\yalFiles\io.class" "%~dp0\..\compiledYals\io.class"
PAUSE