ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION
for /f %%f in ('dir /b "%~dp0..\compiledYals"') do (
	SET name=%%f
	echo.
	echo -----------
	echo Executing !name!
	echo -----------
	java -classpath ..\compiledYals !name:~0,-6!
)
PAUSE