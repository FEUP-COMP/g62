@ECHO OFF

ECHO Testing "array1.yal"
java yal2jvm ../yalFiles/array1.yal

ECHO Testing "array2.yal"
java yal2jvm ../yalFiles/array2.yal

ECHO Testing "aval1.yal"
java yal2jvm ../yalFiles/aval1.yal

ECHO Testing "aval2.yal"
java yal2jvm ../yalFiles/aval2.yal

ECHO Testing "aval3.yal"
java yal2jvm ../yalFiles/aval3.yal

ECHO Testing "aval4.yal"
java yal2jvm ../yalFiles/aval4.yal

ECHO Testing "aval5.yal"
java yal2jvm ../yalFiles/aval5.yal

ECHO Testing "aval6.yal"
java yal2jvm ../yalFiles/aval6.yal

ECHO Testing "aval7.yal"
java yal2jvm ../yalFiles/aval7.yal

ECHO Testing "aval8.yal"
java yal2jvm ../yalFiles/aval8.yal

ECHO Testing "library1.yal"
java yal2jvm ../yalFiles/library1.yal

ECHO Testing "max.yal"
java yal2jvm ../yalFiles/max.yal

ECHO Testing "max_array.yal"
java yal2jvm ../yalFiles/max_array.yal

ECHO Testing "max1.yal"
java yal2jvm ../yalFiles/max1.yal

ECHO Testing "maxmin.yal"
java yal2jvm ../yalFiles/maxmin.yal

ECHO Testing "programa1.yal"
java yal2jvm ../yalFiles/programa1.yal

ECHO Testing "programa2.yal"
java yal2jvm ../yalFiles/programa2.yal

ECHO Testing "programa3.yal"
java yal2jvm ../yalFiles/programa3.yal

ECHO Testing "sqrt.yal"
java yal2jvm ../yalFiles/sqrt.yal

ECHO Finished testing
PAUSE
EXIT