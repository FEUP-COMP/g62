@ECHO OFF

ECHO Testing "array2_err.yal"
java yal2jvm ../yalErrorFiles/array2_err.yal
PAUSE

ECHO Testing "array4_err.yal"
java yal2jvm ../yalErrorFiles/array4_err.yal
PAUSE

ECHO Testing "aval1_err.yal"
java yal2jvm ../yalErrorFiles/aval1_err.yal
PAUSE

ECHO Testing "aval2_err.yal"
java yal2jvm ../yalErrorFiles/aval2_err.yal
PAUSE

ECHO Testing "aval3_err.yal"
java yal2jvm ../yalErrorFiles/aval3_err.yal
PAUSE

ECHO Testing "aval4_err.yal"
java yal2jvm ../yalErrorFiles/aval4_err.yal
PAUSE

ECHO Testing "aval5_err.yal"
java yal2jvm ../yalErrorFiles/aval5_err.yal
PAUSE

ECHO Testing "aval6_err.yal"
java yal2jvm ../yalErrorFiles/aval6_err.yal
PAUSE

ECHO Testing "aval7_err.yal"
java yal2jvm ../yalErrorFiles/aval7_err.yal
PAUSE

ECHO Testing "err1.yal"
java yal2jvm ../yalErrorFiles/err1.yal
ECHO Finished testing
PAUSE