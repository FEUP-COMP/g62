->The following .bat files should work if launched inside of the testsuite folder.

If not working here are the specifics of each:

CompileYal2jvmProject.bat
 - creates a bin folder
 - compiles the yal2jvm project
 
AllFilesFromYalToJasmin.bat
 - runs yal2jvm for every .yal file inside YalFiles

AllFilesFromJasminToJVM.bat
 - generates binary Java class files using the .j files present in jasminFiles
 - the class files go to compiledYals folder
 - moves into that same folder the io.class file
 
RunAllYalPrograms.bat
 - executes all java programs inside compiledYals folder

-----------------------------------------------------------------------------------

->The .bat files described below should work if launched inside of the bin folder.

If not working here are the specifics of each:

DragNDropParse.bat
 - calls "yal2jvm" on the file dropped on top of .bat file
 - needs "yal2jvm.class" and the .yal file on the same folder as the .bat file

DragNDropParse-DEBUG.bat
 - same as above but runs in DEBUG mode, shows total errors expected by looking at filename and assuming that numbers mean the total number of errors

SyntaxTestAll.bat
 - calls "yal2jvm" on every .yal error file available on Moodle
 - needs "yal2jvm.class" on the same folder as the .bat file
 - needs a "yalErrorFiles" folder one level up from the folder .bat file resides in

SyntaxTestAll-DEBUG
 - same as above but runs automatically and stops if any of the .yal files don't return the expected errors

TestCorrectAll
 - calls "yal2jvm" on every correct .yal file available on Moodle
 - needs "yal2jvm.class" on the same folder as the .bat file
 - needs a "yalFiles" folder one level up from the folder .bat file resides in