@ECHO OFF
SET TOTAL=0

ECHO Testing "array2_err.yal"
java yal2jvm -d ../yalErrorFiles/array2_err.yal
IF %ERRORLEVEL% == -1 CALL :do_sum

ECHO Testing "array4_err.yal"
java yal2jvm -d ../yalErrorFiles/array4_err.yal
IF %ERRORLEVEL% == -1 CALL :do_sum

ECHO Testing "aval1_err.yal"
java yal2jvm -d ../yalErrorFiles/aval1_err.yal
IF %ERRORLEVEL% == -1 CALL :do_sum

ECHO Testing "aval2_err.yal"
java yal2jvm -d ../yalErrorFiles/aval2_err.yal
IF %ERRORLEVEL% == -1 CALL :do_sum

ECHO Testing "aval3_err.yal"
java yal2jvm -d ../yalErrorFiles/aval3_err.yal
IF %ERRORLEVEL% == -1 CALL :do_sum

ECHO Testing "aval4_err.yal"
java yal2jvm -d ../yalErrorFiles/aval4_err.yal
IF %ERRORLEVEL% == -1 CALL :do_sum

ECHO Testing "aval5_err.yal"
java yal2jvm -d ../yalErrorFiles/aval5_err.yal
IF %ERRORLEVEL% == -1 CALL :do_sum

ECHO Testing "aval6_err.yal"
java yal2jvm -d ../yalErrorFiles/aval6_err.yal
IF %ERRORLEVEL% == -1 CALL :do_sum

ECHO Testing "aval7_err.yal"
java yal2jvm -d ../yalErrorFiles/aval7_err.yal
IF %ERRORLEVEL% == -1 CALL :do_sum

ECHO Testing "err1.yal"
java yal2jvm -d ../yalErrorFiles/err1.yal
IF %ERRORLEVEL% == -1 CALL :do_sum

ECHO Finished testing, caught all errors in %TOTAL% / 10 files
PAUSE
EXIT

:do_sum
SET /A "TOTAL = TOTAL + 1"
EXIT /B 0