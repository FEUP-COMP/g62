﻿┌──────────────────────────────────┐
│          Final Delivery          │
└──────────────────────────────────┘

**PROJECT TITLE: yal2jvm

**GROUP: g62

  NAME                                    NUMBER        GRADE   CONTRIBUTION
  Catarina Raquel da Silva Ferreira       up201506671   19      25%
  Francisco Teixeira Lopes                up201106912   19      25%
  Matilde Agostinho Moedas Dias Freilão   up201504208   19      25%
  Tiago Costa Neves                       up201506203   19      25%

GLOBAL Grade of the project: 19

**SUMMARY:

  The yal2jvm compiler is capable of compiling .yal language files into Java .class files. To do so, it first runs
  the .yal file through a parser that validates the language structure and checks for syntax errors, while building
  an AST to aid in later compilation stages. In the next stage, the compiler builds the HLIR while also building
  the symbol tables, followed by the semantic analysis. If all these stages were successful the compiler decides
  whether to perform register allocation based on the -r=<num> flag supplied via command line. The method used
  for such allocation is the one taught in the classes. Regardless of using this register allocation method
  or not, the compiler proceeds to the code generation phase and outputs .j files for use with the Jasmin tool.

  Feature keywords: parser, AST, syntactic analysis, HLIR, symbol table, semantic analysis, register allocation, code generation

**EXECUTE:

  If using the .class file
    - java yal2jvm [-r=<num>] [-d] <filepath>
  If using the .jar file
    - java -jar yal2jvm.jar [-r=<num>] [-d] <filepath>

  filepath - the .yal file path to use for compilation
  -r=<num> - a number between 1 and 255 to use for register allocation
  -d       - sets the debug flag for additional prints, can use to see the symbol tables and the register allocation steps

  Brackets indicate optional parameters, the filename is the only mandatory parameter.

**DEALING WITH SYNTACTIC ERRORS:

  Whenever a syntactic error is detected, the parser tries to skip to the next logical token. Some types of errors
  have more advanced handling, such as Call(), Stmt() and Assign() nodes which have context sensitive skips.
  The errors are reported whenever found and no limit is imposed on the number of errors reported.

**SEMANTIC ANALYSIS:

  The following rules have been implemented
    - wrong operand type when assigning to scalar
    - duplicate global declaration
    - duplicate function name declared
    - duplicate function parameter name declared
    - call to undeclared internal function
    - assign variable to incompatible internal function return
    - LHS .size error (i.e. a.size = 5)
    - RHS .size on non arrays
    - error on redeclarating scalar as array (i.e. a = 5; a = [20];)
    - internal function call parameter number mismatch
    - internal function call parameter type mismatch
    - internal function call parameters not declared
    - internal function return type mismatch with returned variable
    - internal function return undeclared in globals / parameters / locals
    - undeclared index on array access
    - undeclared array access on LHS
    - undeclared variables on RHS
    - local variable uninitialized errors (works for arbitrary if/else/while nesting)
    - external function call parameters not declared

  As opposed to the syntactic analysis, this one only reports up to 10 errors.

**INTERMEDIATE REPRESENTATIONS (IRs):

  The HLIR implemented acts as a linear representation of the instructions found in the equivalent .yal file so the
  code generation can just read the file sequentially to generate all code. The particular syntax of the HLIR is
  described in the text file HIR.txt found in the same folder as this README. Since the HLIR is created by
  recursing through the entire AST, it also is used to create the symbol tables and to create the use/def
  sets to use later in the register allocation phase.

**CODE GENERATION:

  Code generation process starts by reading the .bla file (HIR) line by line. Each line has an instruction that can be
  converted to its own set of instructions in Jasmin code. The Jasmin code is always the same for an instruction, so the 
  conversion from HIR instruction to Jasmin code is made using templates. Each variable is parsed using regular expressions. 
  Then, using the name of the variable as a key, the register to use is looked up in the symbol table. 
  Optimizations, like using the instructions with underscore for registers between 0 and 4, instead of always using instructions
  without underscore, and replacing var = var + 1 with iinc, are made.

**OVERVIEW:

  The tool starts by using JavaCC to parse the .yal file and checking for syntactic errors. Followed by the construction
  of the HLIR and symbol tables using the AST created during the parse. After this, a semantic analysis stage runs and if
  it finds no errors the tool proceeds to the register allocation stage. This register allocation stage only occurs if
  the appropriate compiler flag has been set (-r=<num>). Finally the code generation stage takes place, outputting the
  .j files to be used with the Jasmin tool.

  The algorithms worth mentioning are all in the register allocation stage, which follows the method taught in class.
  The compiler computes the use/def sets and the in/out sets for each line and then proceeds to create the interference
  graph. Finally it tries to use graph coloring algorithms to find if the graph is k-colorable, k being the <num> supplied
  in the -r option. If it is k-colorable, it allocates the registers by popping the graph nodes stack sequentially and
  "painting" with the available registers by checking the connecting nodes for conflicting colors.

  The tool uses the JavaCC package to provide parsing capabilities, no other third-party tools or packages are used.

**TESTSUITE AND TEST INFRASTRUCTURE:

  The testsuite folder has several scripts, each described in the README.txt found in that same folder.

**TASK DISTRIBUTION:

  Catarina Raquel da Silva Ferreira
    - Grammar / Parser / AST / Code generation / Symbol table
  Francisco Teixeira Lopes
    - Syntactic analysis / Semantic analysis / Register allocation / HIR / Symbol table
  Matilde Agostinho Moedas Dias Freilão
    - Grammar / Parser / AST / Code generation / Symbol table
  Tiago Costa Neves
    - Grammar / Parser / AST / Code generation / Symbol table

**PROS:

    - Syntactic analysis catches all example errors in provided .yal files. Also catches some additional errors
    - Semantic analysis catches a lot of errors (described in the appropriate section)
    - Code generation 100% complete, with the expected default optimizations (i.e. lower instruction cost selected when possible)
    - Register allocation for local variables using in/out set / interference graph / graph coloring method

**CONS:

    - No additional optimizations (-o compiler flag)
    - Correct .stack / .local values are not computed